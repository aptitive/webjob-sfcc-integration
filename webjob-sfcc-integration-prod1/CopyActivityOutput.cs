﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace webjob_sfcc_integration_prod1
{
    public class CopyActivityOutput
    {
        [JsonProperty("dataRead")]
        public long DataRead { get; set; }

        [JsonProperty("dataWritten")]
        public long DataWritten { get; set; }

        [JsonProperty("filesRead")]
        public int FilesRead { get; set; }

        [JsonProperty("rowsRead")]
        public int RowsRead { get; set; }

        [JsonProperty("rowsCopied")]
        public int RowsCopied { get; set; }

        [JsonProperty("rowsSkipped")]
        public int? RowsSkipped { get; set; }

        [JsonProperty("copyDuration")]
        public int CopyDuration { get; set; }

        [JsonProperty("throughput")]
        public float Throughput { get; set; }

        [JsonProperty("redirectRowPath")]
        public string RedirectRowPath { get; set; }

        [JsonProperty("errors")]
        public string[] Errors { get; set; }

        [JsonProperty("effectiveIntegrationRuntime")]
        public string EffectiveIntegrationRuntime { get; set; }

        [JsonProperty("usedCloudDataMovementUnits")]
        public int UsedCloudDataMovementUnits { get; set; }

        [JsonProperty("usedParallelCopies")]
        public int UsedParallelCopies { get; set; }

        [JsonProperty("executionDetails")]
        public ExecutionDetail[] ExecutionDetails { get; set; }
    }

    public class ExecutionDetail
    {
        [JsonProperty("source")]
        public Source Source { get; set; }

        [JsonProperty("sink")]
        public Sink Sink { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("start")]
        public DateTime Start { get; set; }

        [JsonProperty("duration")]
        public int Duration { get; set; }

        [JsonProperty("usedCloudDataMovementUnits")]
        public int UsedCloudDataMovementUnits { get; set; }

        [JsonProperty("usedParallelCopies")]
        public int UsedParallelCopies { get; set; }

        [JsonProperty("detailedDurations")]
        public DetailedDurations DetailedDurations { get; set; }
    }

    public class Source
    {
        [JsonProperty("type")]
        public string Type { get; set; }
    }

    public class Sink
    {
        [JsonProperty("type")]
        public string Type { get; set; }
    }

    public class DetailedDurations
    {
        [JsonProperty("queuingDuration")]
        public int QueuingDuration { get; set; }

        [JsonProperty("transferDuration")]
        public int TransferDuration { get; set; }
    }
}
