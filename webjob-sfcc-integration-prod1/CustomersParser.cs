﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;

namespace webjob_sfcc_integration_prod1
{
    public class CustomersParser
    {
        //=========================================================== 
        //                  CONSTANTS
        //===========================================================

        // Parsing functions table variables
        const string sourceSchema = "Stage";
        const string sourceTable = "SFCC_CustomersFile";
        const string customersSchema = "Stage";
        const string customersTable = "SFCC_Customers";
        const string customerGroupsSchema = "Stage";
        const string customerGroupsTable = "SFCC_CustomerGroups";
        const string customerGroupsColumnName = "Customer_Groups";
        const string loyaltyTierSchema = "Stage";
        const string loyaltyTierTable = "SFCC_LoyaltyTier";
        const string loyaltyTierColumnName = "Loyalty_Tier";
        const string CDCColumnName = "UpdatedDate";

        // Parsing functions stored procedures
        const string SPExtractCustomers = "[ETL].[Get_SFCC_Customers]";
        const string SPMergeCustomers = "[ETL].[Merge_SFCC_Customers]";
        const string SPMergeCustomerGroups = "[ETL].[Merge_SFCC_CustomerGroups]";
        const string SPMergeLoyaltyTier = "[ETL].[Merge_SFCC_LoyaltyTier]";

        // Stored procedure timeout value in seconds
        const int timeout = 300;

        // Sql Bulk Copy batch size
        const int batchsize = 10000;

        //===========================================================

        
        private SqlConnection conn;
        private Audit audit;
        private Logger logger;
        private CloudQueue emailAlertQueue;
        private long processKey;

        public CustomersParser(SqlConnection _conn, Audit _audit, Logger _logger, CloudQueue _emailAlertQueue, long _processKey)
        {
            this.conn = _conn;
            this.audit = _audit;
            this.logger = _logger;
            this.emailAlertQueue = _emailAlertQueue;
            this.processKey = _processKey;
        }

        public char ExtractCustomers()
        {
            // Initialize ProcessTask key
            long? processTaskKey = null;
            try
            {
                // Insert new record into [Audit].[ProcessTask] table
                processTaskKey = audit.getInitialProcessTaskRecord(processKey, $"Extract Customers data from {sourceSchema}.{sourceTable} into SFCC.Customers");
            }
            catch (Exception e)
            {
                logger.log($"Failed to initialize ProcessTask record for ExtractCustomers parsing function.");
                Console.WriteLine($"Failed to initialize ProcessTask record for ExtractCustomers parsing function.");

                // Write to [Audit].[ErrorLog] table
                audit.writeToErrorLog(processKey, processTaskKey, null, null, "Azure WebJob - SFCC Integration: ExtractCustomers",
                                        $"Extract Customers from Stage.SFCC_CustomersFile into {customersSchema}.{customersTable}", e.Message);

                // send email alert
                emailAlertQueue.AddMessage(new CloudQueueMessage($"Azure WebJob - SFCC Integration: ExtractCustomers;{e};{Convert.ToString(processKey)};;;"));

                // Return fail flag
                return 'N';
            }


            // Initialize ProcessSubtaskKey
            long? processSubtaskKey = null;

            try
            {
                try
                {
                    logger.log($"Extracting Customers data from {sourceSchema}.{sourceTable}...");
                    Console.WriteLine($"Extracting Customers data from {sourceSchema}.{sourceTable}...");

                    // Insert new record into [Audit].[ProcessSubtask] table
                    processSubtaskKey = audit.getInitialProcessSubtaskRecord(processKey, (long)processTaskKey, $"Extract Customers data from {sourceSchema}.{sourceTable} into {customersSchema}.{customersTable}",
                                                                                customersSchema, customersTable, true, sourceSchema, sourceTable, CDCColumnName, false);
                                        
                    // Open SQL connection
                    this.conn.Open();

                    // Extract customers data from [Stage].[SFCC_customersFile] into [Stage].[SFCC_customers]                
                    using (SqlCommand cmd = new SqlCommand(SPExtractCustomers, conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add(new SqlParameter("@AuditProcessKey", processKey));
                        cmd.Parameters.Add(new SqlParameter("@AuditProcessTaskKey", processTaskKey));
                        cmd.Parameters.Add(new SqlParameter("@AuditProcessSubtaskKey", processSubtaskKey));

                        cmd.CommandTimeout = timeout;

                        cmd.ExecuteNonQuery();
                    }

                    // Close SQL connection
                    this.conn.Close();

                    logger.log($"Successfully extracted Customers columns into {customersSchema}.{customersTable}");
                    Console.WriteLine($"Successfully extracted Customers columns into {customersSchema}.{customersTable}");
                }
                catch (Exception e)
                {
                    logger.log($"Customers extraction failed. Details: {e}");
                    Console.WriteLine($"Customers extraction failed. Details: {e}");
                    throw new Exception($"Customers extraction failed. {e}");
                }

                try
                {
                    logger.log($"Collecting transaction metadata...");
                    Console.WriteLine($"Collecting transaction metadata...");

                    // Retrieve transaction metadata
                    int? sourceRowCount = null;
                    int? insertedRowCount = null;
                    int? finalTargetRowCount = null;

                    // Open SQL connection
                    this.conn.Open();

                    using (SqlCommand cmd = new SqlCommand($"SELECT COUNT(1) FROM [{sourceSchema}].[{sourceTable}]", conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                sourceRowCount = Convert.ToInt32(reader[0].ToString());
                            }

                            insertedRowCount = sourceRowCount; // All source rows are inserts
                        }
                    }

                    using (SqlCommand cmd = new SqlCommand($"SELECT COUNT(1) FROM [{customersSchema}].[{customersTable}];", conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                finalTargetRowCount = Convert.ToInt32(reader[0].ToString());
                            }
                        }
                    }

                    // Close SQL connection
                    this.conn.Close();

                    logger.log($"Successfully collected transaction metadata");
                    Console.WriteLine($"Successfully collected transaction metadata");

                    // Update [Audit].[ProcessSubtask] table
                    audit.updateProcessSubtaskRecord((long)processSubtaskKey, 'Y', false, sourceRowCount, 0, (int)finalTargetRowCount, insertedRowCount, 0);
                }
                catch (Exception e)
                {
                    logger.log($"Transaction metadata collection failed. Details: {e}");
                    Console.WriteLine($"Transaction metadata collection failed. Details: {e}");
                    throw new Exception($"Transaction metadata collection failed. {e}");
                }                
            }
            catch (Exception e)
            {
                logger.log($"ExtractCustomers failed to execute successfully. Error: {e}");
                Console.WriteLine($"ExtractCustomers failed to execute successfully. Error: {e}");

                // Update [Audit].[ProcessSubtask] table
                audit.updateProcessSubtaskRecord((long)processSubtaskKey, 'N', false);

                // update [Audit].[ProcessTask] table
                audit.updateProcessTaskRecord((long)processTaskKey, 'N');

                // Write to [Audit].[ErrorLog] table
                audit.writeToErrorLog(processKey, processTaskKey, null, processSubtaskKey, "Azure WebJob - SFCC Integration: ExtractCustomers",
                                        $"Extract Customers from Stage.SFCC_CustomersFile into {customersSchema}.{customersTable}", e.Message);

                // send email
                emailAlertQueue.AddMessage(new CloudQueueMessage($"Azure WebJob - SFCC Integration: ExtractCustomers;{e};{Convert.ToString(processKey)};;{processTaskKey};{processSubtaskKey}"));

                // Return fail flag
                return 'N';
            }
            finally
            {
                // Close SQL connection
                this.conn.Close();
            }

            try
            {
                logger.log($"Executing ETL.Merge_SFCC_Customers...");
                Console.WriteLine($"Executing ETL.Merge_SFCC_Customers...");

                // Insert new record into [Audit].[ProcessSubtask] table
                processSubtaskKey = audit.getInitialProcessSubtaskRecord(processKey, (long)processTaskKey, $"Merge {customersSchema}.{customersTable} into SFCC.Customers",
                                                                            "SFCC", "Customers", true, customersSchema, customersTable, CDCColumnName, false);

                // Open SQL connection
                this.conn.Open();

                using (SqlCommand cmd = new SqlCommand(SPMergeCustomers, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@AuditProcessSubtaskKey", processSubtaskKey));
                    cmd.Parameters.Add(new SqlParameter("@SourceHasAuditColumnsFlag", false));

                    cmd.CommandTimeout = timeout;

                    cmd.ExecuteNonQuery();

                }

                logger.log($"Merge execution successful");
                Console.WriteLine($"Merge execution successful");

                // Close SQL connection
                this.conn.Close();

                // Update [Audit].[ProcessTask] table
                audit.updateProcessTaskRecord((long)processTaskKey, 'Y');
                logger.log($"ExtractCustomers completed successfully");
                Console.WriteLine($"ExtractCustomers completed successfully");

                // Return success flag
                return 'Y';
            }
            catch (Exception e)
            {
                // log exception
                logger.log($"Merge execution failed. Error: {e}");
                Console.WriteLine($"Merge execution failed. Error: {e}");

                // Update [Audit].[ProcessSubtask] table
                audit.updateProcessSubtaskRecord((long)processSubtaskKey, 'N', false);

                // update [Audit].[ProcessTask] table
                audit.updateProcessTaskRecord((long)processTaskKey, 'N');

                // Write to [Audit].[ErrorLog] table
                audit.writeToErrorLog(processKey, processTaskKey, null, processSubtaskKey, "Azure WebJob - SFCC Integration: ExtractCustomers",
                                        $"Extract Customers from {sourceSchema}.{sourceTable} into SFCC.Customers", e.Message);

                // send email
                emailAlertQueue.AddMessage(new CloudQueueMessage($"Azure WebJob - SFCC Integration: ExtractCustomers;{e};{Convert.ToString(processKey)};;{processTaskKey};{processSubtaskKey}"));

                // return fail flag
                return 'N';
            }
            finally
            {
                // Close SQL connection
                this.conn.Close();
            }
        }

        public char ExtractCustomerGroups()
        {
            // Initialize ProcessTask key
            long? processTaskKey = null;
            try
            {
                // Insert new record into [Audit].[ProcessTask] table
                processTaskKey = audit.getInitialProcessTaskRecord(processKey, $"Extract CustomerGroups data and row counts from CustomerGroups csv column in {sourceSchema}.{sourceTable} into SFCC.CustomerGroups");
            }
            catch (Exception e)
            {
                logger.log($"Failed to initialize ProcessTask record for ExtractCustomerGroups parsing function.");
                Console.WriteLine($"Failed to initialize ProcessTask record for ExtractCustomerGroups parsing function.");

                // Write to [Audit].[ErrorLog] table
                audit.writeToErrorLog(processKey, processTaskKey, null, null, "Azure WebJob - SFCC Integration: ExtractCustomerGroups",
                                        $"Extract CustomerGroups from Stage.SFCC_CustomersFile into {customersSchema}.{customersTable}", e.Message);

                // send email alert
                emailAlertQueue.AddMessage(new CloudQueueMessage($"Azure WebJob - SFCC Integration: ExtractCustomerGroups;{e};{Convert.ToString(processKey)};;;"));

                // Return fail flag
                return 'N';
            }

            // Initialize ProcessSubtaskKey
            long? processSubtaskKey = null;

            List<CustomerGroup> customerGroups = new List<CustomerGroup>();

            try
            {
                try
                {
                    logger.log($"Extracting CustomerGroups csv data from {sourceSchema}.{sourceTable}...");
                    Console.WriteLine($"Extracting CustomerGroups csv data from {sourceSchema}.{sourceTable}...");

                    // Insert new record into [Audit].[ProcessSubtask] table
                    processSubtaskKey = audit.getInitialProcessSubtaskRecord(processKey, (long)processTaskKey, $"Extract CustomerGroups csv data from {sourceSchema}.{sourceTable} into {customerGroupsSchema}.{customerGroupsTable}",
                                                                                customerGroupsSchema, customerGroupsTable, true, sourceSchema, sourceTable, CDCColumnName, false);
                                        
                    // Open SQL connection
                    this.conn.Open();

                    // Extract CustomerGroups from [Stage].[SFCC_CustomersFile]
                    string query = $"SELECT [{customerGroupsColumnName}], [Customer_ID] FROM [{sourceSchema}].[{sourceTable}] " +
                                    $"WHERE [{customerGroupsColumnName}] IS NOT NULL AND [Customer_ID] IS NOT NULL;";

                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                if (!reader.IsDBNull(0))
                                {
                                    string[] groups = reader[0].ToString().Split(',');

                                    // Combine group names and customer id into new CustomerGroup object
                                    foreach(string group in groups)
                                    {
                                        customerGroups.Add(new CustomerGroup()
                                        {
                                            CustomerID = reader[1].ToString(),
                                            GroupName = group
                                        });
                                    }
                                }
                            }
                        }
                    }

                    // Close SQL connection
                    this.conn.Close();

                    logger.log($"Successfully extracted CustomerGroups csv data");
                    Console.WriteLine($"Successfully extracted CustomerGroups csv data");
                }
                catch (Exception e)
                {
                    logger.log($"CustomerGroups csv data extraction failed. Details: {e}");
                    Console.WriteLine($"CustomerGroups csv data extraction failed. Details: {e}");
                    throw new Exception($"CustomerGroups csv data extraction failed. Details: {e}");
                }

                try
                {
                    logger.log($"Writing CustomerGroups data to {customerGroupsSchema}.{customerGroupsTable}...");
                    Console.WriteLine($"Writing CustomerGroups data to {customerGroupsSchema}.{customerGroupsTable}...");

                    // Open SQL connection
                    this.conn.Open();

                    // Truncate staging table
                    string query = $"TRUNCATE TABLE [{customerGroupsSchema}].[{customerGroupsTable}];";
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        cmd.ExecuteNonQuery();
                    }

                    // Insert records into database
                    DataTable table = CreateDataTable("CustomerGroups", customerGroups);
                    WriteToDatabase(customerGroupsSchema, customerGroupsTable, table);

                    // Close SQL connection
                    this.conn.Close();

                    Console.WriteLine("Write successful");
                    logger.log($"Write successful");
                }
                catch (Exception e)
                {
                    // Update [Audit].[ProcessSubtask] table
                    audit.updateProcessSubtaskRecord((long)processSubtaskKey, 'N', false);

                    Console.WriteLine("Write failed");
                    logger.log($"Failed to write CustomerGroups data to {customerGroupsSchema}.{customerGroupsTable}. Details: {e}");

                    throw new Exception($"Failed to write CustomerGroups data to {customerGroupsSchema}.{customerGroupsTable}. Details: {e}");
                }

                try
                {
                    logger.log($"Collecting transaction metadata...");
                    Console.WriteLine($"Collecting transaction metadata...");

                    // Retrieve transaction metadata
                    int? sourceRowCount = null;
                    int? insertedRowCount = null;
                    int? finalTargetRowCount = null;

                    // Count source rows
                    // note: the list is counted because the csv list pulled from this column allows for more than one entry per source row.
                    sourceRowCount = customerGroups.Count;

                    // Count inserted rows
                    insertedRowCount = sourceRowCount; // All source rows are inserts                    

                    // Open SQL connection
                    this.conn.Open();
                                        
                    using (SqlCommand cmd = new SqlCommand($"SELECT COUNT(1) FROM [{customerGroupsSchema}].[{customerGroupsTable}];", conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                finalTargetRowCount = Convert.ToInt32(reader[0].ToString());
                            }
                        }
                    }

                    // Close SQL connection
                    this.conn.Close();

                    logger.log($"Successfully collected transaction metadata");
                    Console.WriteLine($"Successfully collected transaction metadata");

                    // Update [Audit].[ProcessSubtask] table
                    audit.updateProcessSubtaskRecord((long)processSubtaskKey, 'Y', false, sourceRowCount, 0, (int)finalTargetRowCount, insertedRowCount, 0);
                }
                catch (Exception e)
                {
                    logger.log($"Transaction metadata collection failed. Details: {e}");
                    Console.WriteLine($"Transaction metadata collection failed. Details: {e}");

                    throw new Exception($"Transaction metadata collection failed. Details: {e}");
                }
            }
            catch (Exception e)
            {
                logger.log($"ExtractCustomerGroups failed to execute successfully.");
                Console.WriteLine($"ExtractCustomerGroups failed to execute successfully.");

                // Update [Audit].[ProcessSubtask] table
                audit.updateProcessSubtaskRecord((long)processSubtaskKey, 'N', false);

                // update [Audit].[ProcessTask] table
                audit.updateProcessTaskRecord((long)processTaskKey, 'N');

                // Write to [Audit].[ErrorLog] table
                audit.writeToErrorLog(processKey, processTaskKey, null, processSubtaskKey, "Azure WebJob - SFCC Integration: ExtractCustomerGroups",
                                        "Extract CustomerGroups data from Stage.SFCC_CustomersFile into SFCC.CustomerGroups", e.Message);

                // Send email
                emailAlertQueue.AddMessage(new CloudQueueMessage($"Azure WebJob - SFCC Integration: ExtractCustomerGroups;{e};{Convert.ToString(processKey)};;{processTaskKey};{processSubtaskKey}"));

                // Return fail flag
                return 'N';
            }
            finally
            {
                // Close SQL connection
                this.conn.Close();
            }

            try
            {
                logger.log($"Executing {SPMergeCustomerGroups}...");
                Console.WriteLine($"Executing {SPMergeCustomerGroups}...");

                // Insert new record into [Audit].[ProcessSubtask] table
                processSubtaskKey = audit.getInitialProcessSubtaskRecord(processKey, (long)processTaskKey, $"Merge {customerGroupsSchema}.{customerGroupsTable} into SFCC.CustomerGroups",
                                                                            "SFCC", "CustomerGroups", true, customerGroupsSchema, customerGroupsTable, CDCColumnName, false);

                // Open SQL connection
                this.conn.Open();

                // Execute merge stored procedure
                using (SqlCommand cmd = new SqlCommand(SPMergeCustomerGroups, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@AuditProcessSubtaskKey", processSubtaskKey));
                    cmd.Parameters.Add(new SqlParameter("@SourceHasAuditColumnsFlag", false));

                    cmd.CommandTimeout = timeout;

                    cmd.ExecuteNonQuery();
                }

                logger.log($"Merge execution successful");
                Console.WriteLine($"Merge execution successful");

                // Close SQL connection
                this.conn.Close();

                // Update [Audit].[ProcessTask] table
                audit.updateProcessTaskRecord((long)processTaskKey, 'Y');

                logger.log($"ExtractCustomerGroups completed successfully");
                Console.WriteLine($"ExtractCustomerGroups completed successfully");

                // Return success flag
                return 'Y';
            }
            catch (Exception e)
            {
                // log exception
                logger.log($"Merge execution failed. Error: {e}");
                Console.WriteLine($"Merge execution failed. Error: {e}");

                // Update [Audit].[ProcessSubtask] table
                audit.updateProcessSubtaskRecord((long)processSubtaskKey, 'N', false);

                // update [Audit].[ProcessTask] table
                audit.updateProcessTaskRecord((long)processTaskKey, 'N');

                // Write to [Audit].[ErrorLog] table
                audit.writeToErrorLog(processKey, processTaskKey, null, processSubtaskKey, "Azure WebJob - SFCC Integration: ExtractCustomerGroups",
                                        "Extract CustomerGroups data from Stage.SFCC_CustomersFile into SFCC.CustomerGroups", e.Message);

                // send email
                emailAlertQueue.AddMessage(new CloudQueueMessage($"Azure WebJob - SFCC Integration: ExtractCustomerGroups;{e};{Convert.ToString(processKey)};;{processTaskKey};{processSubtaskKey}"));

                // Return fail flag
                return 'N';
            }
            finally
            {
                // Close SQL connection
                this.conn.Close();
            }
        }

        public char ExtractLoyaltyTier()
        {
            // Initialize ProcessTask key
            long? processTaskKey = null;
            try
            {
                // Insert new record into [Audit].[ProcessTask] table
                processTaskKey = audit.getInitialProcessTaskRecord(processKey, $"Extract LoyaltyTier data and row counts from LoyaltyTier Json column in {sourceSchema}.{sourceTable} into SFCC.LoyaltyTier");
            }
            catch (Exception e)
            {
                logger.log($"Failed to initialize ProcessTask record for ExtractLoyaltyTier parsing function.");
                Console.WriteLine($"Failed to initialize ProcessTask record for ExtractLoyaltyTier parsing function.");

                // Write to [Audit].[ErrorLog] table
                audit.writeToErrorLog(processKey, processTaskKey, null, null, "Azure WebJob - SFCC Integration: ExtractLoyaltyTier",
                                        $"Extract LoyaltyTier from Stage.SFCC_CustomersFile into {customersSchema}.{customersTable}", e.Message);

                // send email alert
                emailAlertQueue.AddMessage(new CloudQueueMessage($"Azure WebJob - SFCC Integration: ExtractLoyaltyTier;{e};{Convert.ToString(processKey)};;;"));

                // Return fail flag
                return 'N';
            }

            // Initialize ProcessSubtaskKey
            long? processSubtaskKey = null;

            List<LoyaltyTier> loyaltyTiers = new List<LoyaltyTier>();

            try
            {
                try
                {
                    logger.log($"Extracting LoyaltyTier json from {sourceSchema}.{sourceTable}...");
                    Console.WriteLine($"Extracting LoyaltyTier json from {sourceSchema}.{sourceTable}...");

                    // Insert new record into [Audit].[ProcessSubtask] table
                    processSubtaskKey = audit.getInitialProcessSubtaskRecord(processKey, (long)processTaskKey, $"Extract LoyaltyTier Json from {sourceSchema}.{sourceTable} into {loyaltyTierSchema}.{loyaltyTierTable}",
                                                                                loyaltyTierSchema, loyaltyTierTable, true, sourceSchema, sourceTable, CDCColumnName, false);
                                        
                    // Open SQL connection
                    this.conn.Open();

                    // Extract LoyaltyTier from [Stage].[SFCC_CustomersFile]
                    string query = $"SELECT [{loyaltyTierColumnName}], [Customer_ID] FROM [{sourceSchema}].[{sourceTable}] " +
                                    $"WHERE [{loyaltyTierColumnName}] IS NOT NULL AND [Customer_ID] IS NOT NULL;";

                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                if (!reader.IsDBNull(0))
                                {
                                    // Deserialize LoyaltyTier Json string
                                    LoyaltyTierJson json = JsonConvert.DeserializeObject<LoyaltyTierJson>(reader[0].ToString());

                                    // Combine customer ID and json data into new LoyaltyTier object
                                    loyaltyTiers.Add(new LoyaltyTier()
                                    {
                                        CustomerID = reader[1].ToString(),
                                        LoyaltyTierID = json.id,
                                        Name = json.name
                                    });
                                }
                                
                            }
                        }
                    }

                    // Close SQL connection
                    this.conn.Close();

                    logger.log($"Successfully extracted LoyaltyTier json");
                    Console.WriteLine($"Successfully extracted LoyaltyTier json");
                }
                catch (Exception e)
                {
                    logger.log($"LoyaltyTier json extraction failed. Details: {e}");
                    Console.WriteLine($"LoyaltyTier json extraction failed. Details: {e}");
                    throw new Exception($"LoyaltyTier json extraction failed. Details: {e}");
                }

                try
                {
                    logger.log($"Writing LoyaltyTier data to {loyaltyTierSchema}.{loyaltyTierTable}...");
                    Console.WriteLine($"Writing LoyaltyTier data to {loyaltyTierSchema}.{loyaltyTierTable}...");

                    // Open SQL connection
                    this.conn.Open();

                    // Truncate staging table
                    string query = $"TRUNCATE TABLE [{loyaltyTierSchema}].[{loyaltyTierTable}];";
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        cmd.ExecuteNonQuery();
                    }

                    // Insert records into database
                    DataTable table = CreateDataTable("LoyaltyTier", loyaltyTiers);
                    WriteToDatabase(loyaltyTierSchema, loyaltyTierTable, table);

                    // Close SQL connection
                    this.conn.Close();

                    Console.WriteLine("Write successful");
                    logger.log($"Write successful");
                }
                catch (Exception e)
                {
                    // Update [Audit].[ProcessSubtask] table
                    audit.updateProcessSubtaskRecord((long)processSubtaskKey, 'N', false);

                    Console.WriteLine("Write failed");
                    logger.log($"Failed to write LoyaltyTier data to {loyaltyTierSchema}.{loyaltyTierTable}. Details: {e}");

                    throw new Exception($"Failed to write LoyaltyTier data to {loyaltyTierSchema}.{loyaltyTierTable}. Details: {e}");
                }

                try
                {
                    logger.log($"Collecting transaction metadata...");
                    Console.WriteLine($"Collecting transaction metadata...");

                    // Retrieve transaction metadata
                    int? sourceRowCount = null;
                    int? insertedRowCount = null;
                    int? finalTargetRowCount = null;

                    // Count source rows
                    // note: the list is counted because the json arrays pulled from this column allow for more than one entry per source row.
                    sourceRowCount = loyaltyTiers.Count;

                    // Count inserted rows
                    insertedRowCount = sourceRowCount; // All source rows are inserts

                    // Open SQL connection
                    this.conn.Open();
                   
                    using (SqlCommand cmd = new SqlCommand($"SELECT COUNT(1) FROM [{loyaltyTierSchema}].[{loyaltyTierTable}];", conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                finalTargetRowCount = Convert.ToInt32(reader[0].ToString());
                            }
                        }
                    }

                    // Close SQL connection
                    this.conn.Close();

                    logger.log($"Successfully collected transaction metadata");
                    Console.WriteLine($"Successfully collected transaction metadata");

                    // Update [Audit].[ProcessSubtask] table
                    audit.updateProcessSubtaskRecord((long)processSubtaskKey, 'Y', false, sourceRowCount, 0, (int)finalTargetRowCount, insertedRowCount, 0);
                }
                catch (Exception e)
                {
                    logger.log($"Transaction metadata collection failed. Details: {e}");
                    Console.WriteLine($"Transaction metadata collection failed. Details: {e}");

                    throw new Exception($"Transaction metadata collection failed. Details: {e}");
                }
            }
            catch (Exception e)
            {
                logger.log($"ExtractLoyaltyTier failed to execute successfully.");
                Console.WriteLine($"ExtractLoyaltyTier failed to execute successfully.");

                // Update [Audit].[ProcessSubtask] table
                audit.updateProcessSubtaskRecord((long)processSubtaskKey, 'N', false);

                // update [Audit].[ProcessTask] table
                audit.updateProcessTaskRecord((long)processTaskKey, 'N');

                // Write to [Audit].[ErrorLog] table
                audit.writeToErrorLog(processKey, processTaskKey, null, processSubtaskKey, "Azure WebJob - SFCC Integration: ExtractLoyaltyTier",
                                        "Extract LoyaltyTier data from Stage.SFCC_CustomersFile into SFCC.LoyaltyTier", e.Message);

                // Send email
                emailAlertQueue.AddMessage(new CloudQueueMessage($"Azure WebJob - SFCC Integration: ExtractLoyaltyTier;{e};{Convert.ToString(processKey)};;{processTaskKey};{processSubtaskKey}"));

                // Return fail flag
                return 'N';
            }
            finally
            {
                // Close SQL connection
                this.conn.Close();
            }

            try
            {
                logger.log($"Executing {SPMergeLoyaltyTier}...");
                Console.WriteLine($"Executing {SPMergeLoyaltyTier}...");

                // Insert new record into [Audit].[ProcessSubtask] table
                processSubtaskKey = audit.getInitialProcessSubtaskRecord(processKey, (long)processTaskKey, $"Merge {loyaltyTierSchema}.{loyaltyTierTable} into SFCC.LoyaltyTier",
                                                                            "SFCC", "LoyaltyTier", true, loyaltyTierSchema, loyaltyTierTable, CDCColumnName, false);

                // Open SQL connection
                this.conn.Open();

                // Execute merge stored procedure
                using (SqlCommand cmd = new SqlCommand(SPMergeLoyaltyTier, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@AuditProcessSubtaskKey", processSubtaskKey));
                    cmd.Parameters.Add(new SqlParameter("@SourceHasAuditColumnsFlag", false));

                    cmd.CommandTimeout = timeout;

                    cmd.ExecuteNonQuery();

                }

                logger.log($"Merge execution successful");
                Console.WriteLine($"Merge execution successful");

                // Close SQL connection
                this.conn.Close();

                // Update [Audit].[ProcessTask] table
                audit.updateProcessTaskRecord((long)processTaskKey, 'Y');

                logger.log($"ExtractLoyaltyTier completed successfully");
                Console.WriteLine($"ExtractLoyaltyTier completed successfully");

                // Return success flag
                return 'Y';
            }
            catch (Exception e)
            {
                // log exception
                logger.log($"Merge execution failed. Error: {e}");
                Console.WriteLine($"Merge execution failed. Error: {e}");

                // Update [Audit].[ProcessSubtask] table
                audit.updateProcessSubtaskRecord((long)processSubtaskKey, 'N', false);

                // update [Audit].[ProcessTask] table
                audit.updateProcessTaskRecord((long)processTaskKey, 'N');

                // Write to [Audit].[ErrorLog] table
                audit.writeToErrorLog(processKey, processTaskKey, null, processSubtaskKey, "Azure WebJob - SFCC Integration: ExtractLoyaltyTier",
                                        "Extract LoyaltyTier data from Stage.SFCC_CustomersFile into SFCC.LoyaltyTier", e.Message);

                // send email
                emailAlertQueue.AddMessage(new CloudQueueMessage($"Azure WebJob - SFCC Integration: ExtractLoyaltyTier;{e};{Convert.ToString(processKey)};;{processTaskKey};{processSubtaskKey}"));

                // Return fail flag
                return 'N';
            }
            finally
            {
                // Close SQL connection
                this.conn.Close();
            }
        }

        private DataTable CreateDataTable(string source, object data)
        {
            DataTable table = new DataTable();

            switch (source)
            {
                case "LoyaltyTier":
                    List<LoyaltyTier> loyaltyTiers = (List<LoyaltyTier>)data;

                    // Add columns
                    table.Columns.Add("Customer_ID", typeof(string));
                    table.Columns.Add("LoyaltyTier_ID", typeof(string));
                    table.Columns.Add("Name", typeof(string));
                                        
                    // Add rows
                    foreach (LoyaltyTier loyaltyTier in loyaltyTiers)
                    {
                        table.Rows.Add(loyaltyTier.CustomerID,
                                       loyaltyTier.LoyaltyTierID,
                                       loyaltyTier.Name
                                       );
                    }
                    break;

                case "CustomerGroups":
                    List<CustomerGroup> customerGroups = (List<CustomerGroup>)data;

                    // Add columns
                    table.Columns.Add("Customer_ID", typeof(string));
                    table.Columns.Add("CustomerGroup", typeof(string));
                    
                    // Add rows
                    foreach (CustomerGroup group in customerGroups)
                    {
                        table.Rows.Add(group.CustomerID,
                                       group.GroupName 
                                      );
                    }
                    break;
            }

            return table;
        }

        private void WriteToDatabase(string targetSchema, string targetTable, DataTable table)
        {            
            SqlBulkCopy copier = new SqlBulkCopy(conn);

            // Set timeout and batch size
            copier.BatchSize = batchsize;
            copier.BulkCopyTimeout = timeout;

            // Set target
            copier.DestinationTableName = $"{targetSchema}.{targetTable}";

            // Map data table columns to target table columns
            switch (targetTable)
            {
                case "SFCC_LoyaltyTier":
                    copier.ColumnMappings.Add(new SqlBulkCopyColumnMapping("Customer_ID", "Customer_ID"));
                    copier.ColumnMappings.Add(new SqlBulkCopyColumnMapping("LoyaltyTier_ID", "LoyaltyTier_ID"));
                    copier.ColumnMappings.Add(new SqlBulkCopyColumnMapping("Name", "Name"));
                    break;
                case "SFCC_CustomerGroups":
                    copier.ColumnMappings.Add(new SqlBulkCopyColumnMapping("Customer_ID", "Customer_ID"));
                    copier.ColumnMappings.Add(new SqlBulkCopyColumnMapping("CustomerGroup", "CustomerGroup"));
                    break;
            }

            copier.WriteToServer(table);
        }
    }
}
