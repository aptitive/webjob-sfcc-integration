﻿using Newtonsoft.Json;

namespace webjob_sfcc_integration_prod1
{
    public class OrderLineItem
    {
        [JsonProperty("LineItemID")]
        public string LineItemID { get; set; }

        [JsonProperty("OrderNo")]
        public string OrderNo { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("DiscountPrice")]
        public decimal DiscountPrice { get; set; }

        [JsonProperty("ImageURL")]
        public object ImageURL { get; set; }

        [JsonProperty("ItemNo")]
        public string ItemNo { get; set; }

        [JsonProperty("ProductURL")]
        public object ProductURL { get; set; }

        [JsonProperty("QuantityOrdered")]
        public int QuantityOrdered { get; set; }

        [JsonProperty("TotalLineAmount")]
        public decimal TotalLineAmount { get; set; }

        [JsonProperty("UnitPrice")]
        public decimal UnitPrice { get; set; }
    }
}
