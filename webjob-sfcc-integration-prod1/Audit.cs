﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace webjob_sfcc_integration_prod1
{
    public class Audit
    {
        private string _connectionString;

        public Audit(string connectionString)
        {
            _connectionString = connectionString;
        }

        /// <summary>
        /// Inserts a new record in the Audit.Process table and returns an AuditProcessKey
        /// </summary>
        /// <param name="processDescription"></param>
        /// <param name="sourceSystemName"></param>
        /// <param name="toolUsed"></param>
        /// <returns>"AuditProcessKey"</returns>
        public long getInitialProcessRecord(string processDescription, string sourceSystemName, string toolUsed)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                // Create SQL command for stored procedure
                SqlCommand command = new SqlCommand("[Audit].[GetInitialProcessRecord]", connection);
                command.CommandType = CommandType.StoredProcedure;

                // Set input parameters
                command.Parameters.Add(new SqlParameter("@ProcessDesc", processDescription));
                command.Parameters.Add(new SqlParameter("@SourceSystemName", sourceSystemName));
                command.Parameters.Add(new SqlParameter("@ToolUsed", toolUsed));

                // Set output parameters
                SqlParameter auditProcessKey = new SqlParameter("@AuditProcessKey", SqlDbType.BigInt);
                auditProcessKey.Direction = ParameterDirection.Output;
                command.Parameters.Add(auditProcessKey);
                
                // Execute stored procedure
                command.ExecuteNonQuery();

                connection.Close();

                return Convert.ToInt64(auditProcessKey.Value);
            }
        }

        /// <summary>
        /// Inserts a new record in the Audit.ProcessFileTask table and returns an AuditProcessFileTaskKey
        /// </summary>
        /// <param name="auditProcessKey"></param>
        /// <param name="sourceFileName"></param>
        /// <param name="sourceFileType"></param>
        /// <param name="sourceFileSize"></param>
        /// <param name="sourceFileLocation"></param>
        /// <returns></returns>
        public long getInitialProcessFileTaskRecord(long auditProcessKey, string sourceFileName, string sourceFileType,
                                                        string sourceFileSize, string sourceFileLocation)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                // Create SQL command for stored procedure
                SqlCommand command = new SqlCommand("[Audit].[GetInitialProcessFileTaskRecord]", connection);
                command.CommandType = CommandType.StoredProcedure;

                // Set input parameters
                command.Parameters.Add(new SqlParameter("@AuditProcessKey", auditProcessKey));
                command.Parameters.Add(new SqlParameter("@SourceFileName", sourceFileName));
                command.Parameters.Add(new SqlParameter("@SourceFileType", sourceFileType));
                command.Parameters.Add(new SqlParameter("@SourceFileSize", sourceFileSize));
                command.Parameters.Add(new SqlParameter("@SourceFileLocation", sourceFileLocation));

                // Set output parameters
                SqlParameter auditProcessFileTaskKey = new SqlParameter("@AuditProcessFileTaskKey", SqlDbType.Int);
                auditProcessFileTaskKey.Direction = ParameterDirection.Output;
                command.Parameters.Add(auditProcessFileTaskKey);

                // Execute stored procedure
                command.ExecuteNonQuery();

                connection.Close();

                return Convert.ToInt64(auditProcessFileTaskKey.Value);
            }
        }

        /// <summary>
        /// Inserts a new record in the Audit.ProcessTask table and returns an AuditProcessTaskKey
        /// </summary>
        /// <param name="auditProcessKey"></param>
        /// <param name="taskDescription"></param>
        /// <returns></returns>
        public long getInitialProcessTaskRecord(long auditProcessKey, string taskDescription)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                // Create SQL command for stored procedure
                SqlCommand command = new SqlCommand("[Audit].[GetInitialProcessTaskRecord]", connection);
                command.CommandType = CommandType.StoredProcedure;

                // Set input parameters
                command.Parameters.Add(new SqlParameter("@AuditProcessKey", auditProcessKey));
                command.Parameters.Add(new SqlParameter("@TaskDesc", taskDescription));

                // Set output parameters
                SqlParameter auditProcessTaskKey = new SqlParameter("@AuditProcessTaskKey", SqlDbType.Int);
                auditProcessTaskKey.Direction = ParameterDirection.Output;
                command.Parameters.Add(auditProcessTaskKey);

                // Execute stored procedure
                command.ExecuteNonQuery();

                connection.Close();

                return Convert.ToInt64(auditProcessTaskKey.Value);
            }
        }

        /// <summary>
        /// Inserts a new record with an AuditProcessFileTaskKey in the Audit.ProcessTask table and returns an AuditProcessTaskKey
        /// </summary>
        /// <param name="auditProcessKey"></param>
        /// <param name="taskDescription"></param>
        /// <param name="auditProcessFileTaskKey"></param>
        /// <returns></returns>
        public long getInitialProcessTaskRecord(long auditProcessKey, string taskDescription, long auditProcessFileTaskKey)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                // Create SQL command for stored procedure
                SqlCommand command = new SqlCommand("[Audit].[GetInitialProcessTaskRecord]", connection);
                command.CommandType = CommandType.StoredProcedure;

                // Set input parameters
                command.Parameters.Add(new SqlParameter("@AuditProcessKey", auditProcessKey));
                command.Parameters.Add(new SqlParameter("@TaskDesc", taskDescription));
                command.Parameters.Add(new SqlParameter("@AuditProcessFileTaskKey", auditProcessFileTaskKey));

                // Set output parameters
                SqlParameter auditProcessTaskKey = new SqlParameter("@AuditProcessTaskKey", SqlDbType.Int);
                auditProcessTaskKey.Direction = ParameterDirection.Output;
                command.Parameters.Add(auditProcessTaskKey);

                // Execute stored procedure
                command.ExecuteNonQuery();

                connection.Close();

                return Convert.ToInt64(auditProcessTaskKey.Value);
            }
        }

        /// <summary>
        /// Inserts a new record in the Audit.ProcessSubtask table and returns an AuditProcessSubtaskKey
        /// </summary>
        /// <param name="auditProcessKey"></param>
        /// <param name="auditProcessTaskKey"></param>
        /// <param name="subtaskDescription"></param>
        /// <param name="targetTableSchema"></param>
        /// <param name="targetTableName"></param>
        /// <param name="sourceIsFile"></param>
        /// <param name="auditProcessFileTaskKey"></param>
        /// <param name="CDCColumnName"></param>
        /// <param name="sourceHasAuditColumns"></param>
        /// <returns></returns>
        public long getInitialProcessSubtaskRecord(long auditProcessKey, long auditProcessTaskKey, string subtaskDescription, string targetTableSchema,
                                                        string targetTableName, bool sourceIsFile, long auditProcessFileTaskKey, string CDCColumnName, bool sourceHasAuditColumns)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                // Create SQL command for stored procedure 
                SqlCommand command = new SqlCommand("[Audit].[GetInitialProcessSubtaskRecord]", connection);
                command.CommandType = CommandType.StoredProcedure;

                // Set input parameters
                command.Parameters.Add(new SqlParameter("@AuditProcessKey", auditProcessKey));
                command.Parameters.Add(new SqlParameter("@AuditProcessTaskKey", auditProcessTaskKey));
                command.Parameters.Add(new SqlParameter("@SubtaskDesc", subtaskDescription));
                command.Parameters.Add(new SqlParameter("@TargetTableSchema", targetTableSchema));
                command.Parameters.Add(new SqlParameter("@TargetTableName", targetTableName));
                command.Parameters.Add(new SqlParameter("@SourceIsFileFlag", sourceIsFile));
                command.Parameters.Add(new SqlParameter("@AuditProcessFileTaskKey", auditProcessFileTaskKey));
                command.Parameters.Add(new SqlParameter("@CDCColumnName", CDCColumnName));
                command.Parameters.Add(new SqlParameter("@SourceHasAuditColumnsFlag", sourceHasAuditColumns));

                // Set output parameters
                SqlParameter auditProcessSubtaskKey = new SqlParameter("@AuditProcessSubtaskKey", SqlDbType.Int);
                auditProcessSubtaskKey.Direction = ParameterDirection.Output;
                command.Parameters.Add(auditProcessSubtaskKey);

                // Execute stored procedure
                command.ExecuteNonQuery();

                connection.Close();

                return Convert.ToInt64(auditProcessSubtaskKey.Value);
            }
        }

        /// <summary>
        /// Inserts a new record in the Audit.ProcessSubtask table and returns an AuditProcessSubtaskKey
        /// </summary>
        /// <param name="auditProcessKey"></param>
        /// <param name="auditProcessTaskKey"></param>
        /// <param name="subtaskDescription"></param>
        /// <param name="targetTableSchema"></param>
        /// <param name="targetTableName"></param>
        /// <param name="sourceIsFile"></param>
        /// <param name="sourceTableSchema"></param>
        /// <param name="sourceTableName"></param>
        /// <param name="CDCColumnName"></param>
        /// <param name="sourceHasAuditColumns"></param>
        /// <returns></returns>
        public long getInitialProcessSubtaskRecord(long auditProcessKey, long auditProcessTaskKey, string subtaskDescription, string targetTableSchema, string targetTableName,
                                                        bool sourceIsFile, string sourceTableSchema, string sourceTableName, string CDCColumnName, bool sourceHasAuditColumns)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                // Create SQL command for stored procedure
                SqlCommand command = new SqlCommand("[Audit].[GetInitialProcessSubtaskRecord]", connection);
                command.CommandType = CommandType.StoredProcedure;

                // Set input parameters
                command.Parameters.Add(new SqlParameter("@AuditProcessKey", auditProcessKey));
                command.Parameters.Add(new SqlParameter("@AuditProcessTaskKey", auditProcessTaskKey));
                command.Parameters.Add(new SqlParameter("@SubtaskDesc", subtaskDescription));
                command.Parameters.Add(new SqlParameter("@TargetTableSchema", targetTableSchema));
                command.Parameters.Add(new SqlParameter("@TargetTableName", targetTableName));
                command.Parameters.Add(new SqlParameter("@SourceIsFileFlag", sourceIsFile));
                command.Parameters.Add(new SqlParameter("@SourceTableSchema", sourceTableSchema));
                command.Parameters.Add(new SqlParameter("@SourceTableName", sourceTableName));
                command.Parameters.Add(new SqlParameter("@CDCColumnName", CDCColumnName));
                command.Parameters.Add(new SqlParameter("@SourceHasAuditColumnsFlag", sourceHasAuditColumns));

                // Set output parameters
                SqlParameter auditProcessSubtaskKey = new SqlParameter("@AuditProcessSubtaskKey", SqlDbType.Int);
                auditProcessSubtaskKey.Direction = ParameterDirection.Output;
                command.Parameters.Add(auditProcessSubtaskKey);

                // Execute stored procedure
                command.ExecuteNonQuery();

                connection.Close();

                return Convert.ToInt64(auditProcessSubtaskKey.Value);
            }
        }

        /// <summary>
        /// Updates existing audit process record in the Audit.Process table
        /// </summary>
        /// <param name="auditProcessKey"></param>
        /// <param name="successFlag"></param>
        public void updateProcessRecord(long auditProcessKey, char successFlag, string comment = null)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                // If comments exist, use the UpdateProcessRecordWithComments stored procedure
                string storedProcedure = null;
                if (!String.IsNullOrEmpty(comment))
                    storedProcedure = "[Audit].[UpdateProcessRecordWithComments]";
                else
                    storedProcedure = "[Audit].[UpdateProcessRecord]";

                // Create SQL command for stored procedure
                SqlCommand command = new SqlCommand(storedProcedure, connection);
                command.CommandType = CommandType.StoredProcedure;

                // Set input parameters (add comments only if not null)
                command.Parameters.Add(new SqlParameter("@AuditProcessKey", auditProcessKey));
                command.Parameters.Add(new SqlParameter("@SuccessFlag", successFlag));
                if (!String.IsNullOrEmpty(comment)) command.Parameters.Add(new SqlParameter("@Comments", comment));

                // Execute stored procedure
                command.ExecuteNonQuery();

                connection.Close();
            }
        }

        /// <summary>
        /// Updates existing audit process file task record in the Audit.ProcessFileTask table
        /// </summary>
        /// <param name="auditProcessKey"></param>
        /// <param name="auditProcessFileTaskKey"></param>
        /// <param name="pipelineName"></param>
        /// <param name="successFlag"></param>
        /// <param name="appKey"></param>
        public void updateProcessFileTaskRecord(long auditProcessKey, long auditProcessFileTaskKey, string pipelineName, char successFlag, long? appKey = null)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                // Create SQL command for stored procedure
                SqlCommand command = new SqlCommand("[Audit].[UpdateProcessFileTaskRecord]", connection);
                command.CommandType = CommandType.StoredProcedure;

                // Set input parameters (add appKey if not null)
                command.Parameters.Add(new SqlParameter("@AuditProcessKey", auditProcessKey));
                command.Parameters.Add(new SqlParameter("@AuditProcessFileTaskKey", auditProcessFileTaskKey));
                command.Parameters.Add(new SqlParameter("@PipelineName", pipelineName));
                command.Parameters.Add(new SqlParameter("@SuccessFlag", successFlag));
                if (appKey != null) command.Parameters.Add(new SqlParameter("@AppKey", appKey));

                // Execute stored procedure
                command.ExecuteNonQuery();

                connection.Close();
            }
        }

        /// <summary>
        /// Updates existing audit process task record in the Audit.ProcessTask table
        /// </summary>
        /// <param name="auditProcessTaskKey"></param>
        /// <param name="successFlag"></param>
        public void updateProcessTaskRecord(long auditProcessTaskKey, char successFlag)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                // Create SQL command for stored procedure
                SqlCommand command = new SqlCommand("[Audit].[UpdateProcessTaskRecord]", connection);
                command.CommandType = CommandType.StoredProcedure;

                // Set input parameters
                command.Parameters.Add(new SqlParameter("@AuditProcessTaskKey", auditProcessTaskKey));
                command.Parameters.Add(new SqlParameter("@SuccessFlag", successFlag));

                // Execute stored procedure
                command.ExecuteNonQuery();

                connection.Close();
            }
        }

        /// <summary>
        /// Updates existing process subtask record in the Audit.ProcessSubtaskRecord table
        /// </summary>
        /// <param name="auditProcessSubtaskKey"></param>
        /// <param name="successFlag"></param>
        /// <param name="sourceHasAuditColumns"></param>
        /// <param name="sourceTableRowCount"></param>
        /// <param name="initialTargetTableRowCount"></param>
        /// <param name="finalTargetTableRowCount"></param>
        /// <param name="insertedRowCount"></param>
        /// <param name="updatedRowCount"></param>
        /// <param name="cdcMaxValue"></param>
        /// <param name="cdcMinValue"></param>
        public void updateProcessSubtaskRecord(long auditProcessSubtaskKey, char successFlag, bool sourceHasAuditColumns, int? sourceTableRowCount = null, int? initialTargetTableRowCount = null, 
                                                int? finalTargetTableRowCount = null, int? insertedRowCount = null, int? updatedRowCount = null, DateTime? cdcMaxValue = null, DateTime? cdcMinValue = null)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();


                // Create SQL command for stored procedure
                SqlCommand command = new SqlCommand("[Audit].[UpdateProcessSubtaskRecord]", connection);
                command.CommandType = CommandType.StoredProcedure;

                // Set input parameters
                command.Parameters.Add(new SqlParameter("@AuditProcessSubtaskKey", auditProcessSubtaskKey));
                command.Parameters.Add(new SqlParameter("@SuccessFlag", successFlag));
                command.Parameters.Add(new SqlParameter("@SourceTableRowCount", sourceTableRowCount));
                command.Parameters.Add(new SqlParameter("@InitialTargetTableRowCount", initialTargetTableRowCount));
                command.Parameters.Add(new SqlParameter("@FinalTargetTableRowCount", finalTargetTableRowCount));
                command.Parameters.Add(new SqlParameter("@InsertedRowCount", insertedRowCount));
                command.Parameters.Add(new SqlParameter("@UpdatedRowCount", updatedRowCount));
                command.Parameters.Add(new SqlParameter("@SourceHasAuditColumnsFlag", sourceHasAuditColumns));
                command.Parameters.Add(new SqlParameter("@CDCMaxValue", cdcMaxValue));

                // Execute stored procedure
                command.ExecuteNonQuery();

                connection.Close();
            }
        }

        /// <summary>
        /// Inserts new error record into the Audit.ErrorLog table
        /// </summary>
        /// <param name="auditProcessKey"></param>
        /// <param name="auditProcessTaskKey"></param>
        /// <param name="auditProcessFileTaskKey"></param>
        /// <param name="auditProcessSubtaskKey"></param>
        /// <param name="procedureName"></param>
        /// <param name="procedureDescription"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errorSeverity"></param>
        /// <param name="errorState"></param>
        public void writeToErrorLog(long? auditProcessKey = null, long? auditProcessTaskKey = null, long? auditProcessFileTaskKey = null, long? auditProcessSubtaskKey = null, string procedureName = null,
                                        string procedureDescription = null, string errorMessage = null, int? errorSeverity = null, int? errorState = null)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                // Create SQL command for stored procedure
                SqlCommand command = new SqlCommand("[Audit].[WriteToErrorLog]", connection);
                command.CommandType = CommandType.StoredProcedure;

                // Set input parameters
                command.Parameters.Add(new SqlParameter("@AuditProcessKey", auditProcessKey));
                command.Parameters.Add(new SqlParameter("@AuditProcessTaskKey", auditProcessTaskKey));
                command.Parameters.Add(new SqlParameter("@AuditProcessFileTaskKey", auditProcessFileTaskKey));
                command.Parameters.Add(new SqlParameter("@AuditProcessSubtaskKey", auditProcessSubtaskKey));
                command.Parameters.Add(new SqlParameter("@ProcedureName", procedureName));
                command.Parameters.Add(new SqlParameter("@ProcedureDesc", procedureDescription));
                command.Parameters.Add(new SqlParameter("@ErrorMessage", errorMessage));
                command.Parameters.Add(new SqlParameter("@ErrorSeverity", errorSeverity));
                command.Parameters.Add(new SqlParameter("@ErrorState", errorState));                          

                // Execute stored procedure
                command.ExecuteNonQuery();

                connection.Close();
            }
        }
    }
}
