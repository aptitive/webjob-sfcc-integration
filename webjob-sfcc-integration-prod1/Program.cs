﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using Microsoft.Azure.Management.DataFactory;
using Microsoft.Azure.Management.DataFactory.Models;
using Newtonsoft.Json;

namespace webjob_sfcc_integration_prod1
{
    class Program
    {        
        static void Main()
        {
            // Local/Production connection strings toggle
            bool isLocal = false;

            // Execute CRM Toggle
            bool executeCRM = false;

            SFCCIntegration(isLocal, executeCRM);

            // Uncomment for local host debugging
            //Console.WriteLine("Done");
            //Console.ReadKey();
        }

        public static void SFCCIntegration(bool isLocal, bool executeCRM)
        {
            Console.WriteLine($"C# Timer trigger function executed at: {DateTime.Now}");

            //=========================================================== 
            //                  CONSTANTS
            //===========================================================

            // Audit variables
            const string process = "SFCC Integration";
            const string processDescription = "Load SFCC flat files into SFCC raw layer";
            const string sourceSystemName = "SFCC";
            const string toolUsed = "Data Factory";

            // Files marked for parsing
            const string orders = "ordersFeed";
            const string customers = "customersFeed";

            //=========================================================== 
            //                  CONFIG FILE
            //===========================================================

            string fileMapAddressKey = String.Empty;
            string parameterAddressKey = String.Empty;
            string ordersFeedFilePathKey = String.Empty;
            string customersFeedFilePathKey = String.Empty;
            string catalogFeedFilePathKey = String.Empty;

            if (isLocal)
            {
                fileMapAddressKey = "FileMapAddress_local";
                parameterAddressKey = "ParametersAddress_local";
                ordersFeedFilePathKey = "OrdersFeedColumnsListAddress_local";
                customersFeedFilePathKey = "CustomersFeedColumnsListAddress_local";
                catalogFeedFilePathKey = "CatalogFeedColumnsListAddress_local";
            }
            else
            {
                fileMapAddressKey = "FileMapAddress_prod";
                parameterAddressKey = "ParametersAddress_prod";
                ordersFeedFilePathKey = "OrdersFeedColumnsListAddress_prod";
                customersFeedFilePathKey = "CustomersFeedColumnsListAddress_prod";
                catalogFeedFilePathKey = "CatalogFeedColumnsListAddress_prod";
            }

            // Json config files
            string fileMapAddress = ConfigurationManager.AppSettings[fileMapAddressKey].ToString();
            string parametersAddress = ConfigurationManager.AppSettings[parameterAddressKey].ToString();
            string ordersFeedFilePath = ConfigurationManager.AppSettings[ordersFeedFilePathKey].ToString();
            string customersFeedFilePath = ConfigurationManager.AppSettings[customersFeedFilePathKey].ToString();
            string catalogFeedFilePath = ConfigurationManager.AppSettings[catalogFeedFilePathKey].ToString();

            // SFTP Credentials
            SftpCredentials sftpcreds = new SftpCredentials()
            {
                host = ConfigurationManager.AppSettings["SFCC_SFTPHost"].ToString(),
                username = ConfigurationManager.AppSettings["SFCC_SFTPUsername"].ToString(),
                password = ConfigurationManager.AppSettings["SFCC_SFTPPassword"].ToString(),
                remoteDirectory = ConfigurationManager.AppSettings["SFCC_SFTPRemoteDirectory"].ToString(),
            };
            string fileType = ConfigurationManager.AppSettings["FileType"].ToString();

            // Azure AD app credentials    
            string tenantID = ConfigurationManager.AppSettings["TenantID"].ToString();
            string applicationId = ConfigurationManager.AppSettings["ApplicationID"].ToString();
            string authenticationKey = ConfigurationManager.AppSettings["AuthenticationKey"].ToString();
            string subscriptionId = ConfigurationManager.AppSettings["SubscriptionID"].ToString();
            string resourceGroup = ConfigurationManager.AppSettings["ResourceGroup"].ToString();
            string dataFactoryName = ConfigurationManager.AppSettings["DataFactoryName"].ToString();

            // Connection strings
            string sqlConnectionString = ConfigurationManager.ConnectionStrings["AzSqlConnectionString"].ConnectionString;
            string storageAccountString = ConfigurationManager.ConnectionStrings["AzureWebJobsStorage"].ConnectionString;

            // Log parameters
            string logContainer = ConfigurationManager.AppSettings["Log_BlobContainer"].ToString();
            string logName = ConfigurationManager.AppSettings["Log_FileName"].ToString();

            // Max retries for transient errors
            int maxRetryCount = Convert.ToInt32(ConfigurationManager.AppSettings["MaxRetryCount"].ToString());

            //=========================================================== 
            //                  BEGIN EXECUTION
            //===========================================================

            // Create archive and email queues
            CloudQueueClient queueClient = CloudStorageAccount.Parse(storageAccountString).CreateCloudQueueClient();
            CloudQueue archiveFileQueue = queueClient.GetQueueReference("sfcc-archivefile");
            CloudQueue emailAlertQueue = queueClient.GetQueueReference("emailalert");

            // Create Logger
            Logger logger = new Logger(logName, storageAccountString, logContainer);
            logger.log($"Azure WebJob - {process} began execution");
            Console.WriteLine($"Azure WebJob - {process} began execution");

            // Create an Audit object for audit records
            Audit audit = new Audit(sqlConnectionString);

            // Insert new record into [Audit].[Process] table and return AuditProcessKey
            long? processKey = null;
            try
            {
                processKey = audit.getInitialProcessRecord(processDescription, sourceSystemName, toolUsed);
            }
            catch (Exception e)
            {
                logger.log($"Azure WebJob - {process} failed to initialize process record. {e}");
                Console.WriteLine($"Azure WebJob - {process} failed to initialize process record. {e}");
                
				try
                {
                    emailAlertQueue.AddMessage(new CloudQueueMessage($"{process};Azure WebJob - {process} failed to initialize process record. {e};;;;"));
                }
                catch (Exception ex)
                {
                    logger.log(ex.ToString());
                    Console.WriteLine(ex.ToString());
                }

                // Stop application if AuditProcessKey failed to initialize
                throw new Exception();
            }

            // Initialize process status flag
            char? processStatusFlag = null;

            logger.log($"{process}: Retrieving file list...");
            Console.WriteLine($"{process}: Retrieving file list...");

            // Get expected columns configurations for file validation
            Dictionary<string, string[]> columnsCatalog = new Dictionary<string, string[]>();
            columnsCatalog.Add("ordersFeed", Helper.GetColumnsList(ordersFeedFilePath));
            columnsCatalog.Add("customersFeed", Helper.GetColumnsList(customersFeedFilePath));
            columnsCatalog.Add("catalogFeed", Helper.GetColumnsList(catalogFeedFilePath));

            // Connect to file service and retrieve file names and metadata
            List<File> files = new List<File>();
            for (int tryCount = 1; tryCount < maxRetryCount + 1; tryCount++) // For loop for transient fault handling
            {
                try
                {
                    files = Helper.getSFTPFiles(sftpcreds, fileType);

                    logger.log($"{files.Count()} files were found for {sourceSystemName}");
                    Console.WriteLine($"{files.Count()} files were found for {sourceSystemName}");                    

                    break;
                }
                catch // Failed to connect
                {
                    if (tryCount < maxRetryCount)
                    {
                        logger.log($"{process} cannot connect to {sftpcreds.host}. Retrying connection in {tryCount} minutes...");
                        Console.WriteLine($"{process} Cannot connect to {sftpcreds.host}. Retrying connection in {tryCount} minutes...");

                        // Wait tryCount number of minutes
                        System.Threading.Thread.Sleep(tryCount * 60000);
                    }
                    else
                    {
                        logger.log($"{process} failed to connect to {sftpcreds.host}. Aborting operation");
                        Console.WriteLine($"{process} failed to connect to {sftpcreds.host}. Aborting operation");

                        try
                        {
                            // Update [Audit].[Process] record
                            audit.updateProcessRecord((long)processKey, 'N');

                            // Write to [Audit].[ErrorLog]
                            audit.writeToErrorLog(processKey, null, null, null, process, processDescription, $"Failed to connect to {sftpcreds.host}");
                        }
                        catch (Exception e)
                        {
                            logger.log(e.ToString());
                            Console.WriteLine(e.ToString());
                        }

                        try
                        {
                            // Send email alert
                            emailAlertQueue.AddMessage(new CloudQueueMessage($"{process};Failed to connect to {sftpcreds.host};{Convert.ToString(processKey)};;;"));
                        }
                        catch (Exception e)
                        {
                            logger.log(e.ToString());
                            Console.WriteLine(e.ToString());
                        }

                        // Stop application if unable to access files
                        throw new Exception();
                    }
                }
            }

            foreach (File file in files)
            {
                for (int tryCount = 1; tryCount < maxRetryCount + 1; tryCount++) // For loop for transient fault handling
                {
                    try
                    {
                        // Validate file schemas
                        string failedValidationMessage = Helper.ValidateFileSchema(file, columnsCatalog, sftpcreds);

                        if (!String.IsNullOrEmpty(failedValidationMessage))
                        {
                            logger.log($"{failedValidationMessage}");
                            Console.WriteLine($"{failedValidationMessage}");

                            // Flag file for exclusion from proceeding operations
                            file.SkipFlag = true;

                            try
                            {
                                // Update [Audit].[Process] record
                                audit.updateProcessRecord((long)processKey, 'N');

                                // Write to [Audit].[ErrorLog]
                                audit.writeToErrorLog(processKey, null, null, null, process, processDescription, $"{failedValidationMessage}");
                            }
                            catch (Exception ex)
                            {
                                logger.log(ex.ToString());
                                Console.WriteLine(ex.ToString());
                            }

                            try
                            {
                                // Send unrecognized file to error directory
                                archiveFileQueue.AddMessage(new CloudQueueMessage($"0;{file.Name};{file.RootName}"));
                            }
                            catch (Exception ex)
                            {
                                logger.log($"{process} failed to archive file {file.Name}. {ex}");
                                Console.WriteLine($"{process} failed to archive file {file.Name}. {ex}");

                                try
                                {
                                    emailAlertQueue.AddMessage(new CloudQueueMessage($"{process};{process} failed to archive file {file.Name};{Convert.ToString(processKey)};{Convert.ToString(file.AuditProcessFileTaskKey)};;"));
                                }
                                catch (Exception exc)
                                {
                                    logger.log(exc.ToString());
                                    Console.WriteLine(exc.ToString());
                                }
                            }

                            try
                            {
                                // Send email alert
                                emailAlertQueue.AddMessage(new CloudQueueMessage($"{process};{failedValidationMessage};{Convert.ToString(processKey)};;;"));
                            }
                            catch (Exception ex)
                            {
                                logger.log(ex.ToString());
                                Console.WriteLine(ex.ToString());
                            }
                        };

                        break;
                    }
                    catch // Failed to connect
                    {
                        if (tryCount < maxRetryCount)
                        {
                            logger.log($"{process} cannot connect to {sftpcreds.host}. Retrying connection in {tryCount} minutes...");
                            Console.WriteLine($"{process} Cannot connect to {sftpcreds.host}. Retrying connection in {tryCount} minutes...");

                            // Wait tryCount number of minutes
                            System.Threading.Thread.Sleep(tryCount * 60000);
                        }
                        else
                        {
                            logger.log($"{process} failed to connect to {sftpcreds.host}. Aborting operation");
                            Console.WriteLine($"{process} failed to connect to {sftpcreds.host}. Aborting operation");

                            try
                            {
                                // Update [Audit].[Process] record
                                audit.updateProcessRecord((long)processKey, 'N');

                                // Write to [Audit].[ErrorLog]
                                audit.writeToErrorLog(processKey, null, null, null, process, processDescription, $"Failed to connect to {sftpcreds.host}");
                            }
                            catch (Exception e)
                            {
                                logger.log(e.ToString());
                                Console.WriteLine(e.ToString());
                            }

                            try
                            {
                                // Send email alert
                                emailAlertQueue.AddMessage(new CloudQueueMessage($"{process};Failed to connect to {sftpcreds.host};{Convert.ToString(processKey)};;;"));
                            }
                            catch (Exception e)
                            {
                                logger.log(e.ToString());
                                Console.WriteLine(e.ToString());
                            }

                            // Stop application if unable to access files
                            throw new Exception();
                        }
                    }
                }
            }

            // If no files were found, end operations
            if (files.Count == 0)
            {
                try
                {
                    // Update [Audit].[Process] record
                    audit.updateProcessRecord((long)processKey, 'Y', "No files found");
                }
                catch (Exception e)
                {
                    logger.log($"Azure WebJob - {process} failed to update process record. {e}");
                    Console.WriteLine($"Azure WebJob - {process} failed to update process record. {e}");

					try
                    {
                        emailAlertQueue.AddMessage(new CloudQueueMessage($"{process};Azure WebJob - {process} failed to update process record. {e};{processKey};;;"));
                    }
                    catch (Exception ex)
                    {
                        logger.log(ex.ToString());
                        Console.WriteLine(ex.ToString());
                    }
				}

                // End application
                return;
            }            

            // Load filemap.json
            Dictionary<string, string> fileMap = Helper.loadFileMap(fileMapAddress);
           
            // Map files to their designated pipeline
            foreach (File file in files)
            {
                try
                {
                    // Insert new record into [Audit].[ProcessFileTask] table and return AuditProcessFileTaskKey
                    file.AuditProcessFileTaskKey = audit.getInitialProcessFileTaskRecord((long)processKey, file.Name, file.Type, file.Size, file.Location);
                }
                catch (Exception e)
                {
                    logger.log($"{process} failed to initialize processtask record for {file.Name}. {e}");
                    Console.WriteLine($"{process} failed to initialize processtask record for {file.Name}. {e}");

                    try
                    {
                        // Send email notification
                        emailAlertQueue.AddMessage(new CloudQueueMessage($"{process};Azure WebJob - {process} failed to initialize processtask record for {file.Name};{processKey};{file.AuditProcessFileTaskKey};;"));
                    }
                    catch (Exception ex)
                    {
                        logger.log(ex.ToString());
                        Console.WriteLine(ex.ToString());
                    }

                    // Flag file for exclusion from proceeding operations
                    file.SkipFlag = true;

                    // Fail process record
                    processStatusFlag = 'N';

                    // Skip to next file
                    continue;
                }

                try
                {
                    // Select designated pipeline
                    file.Pipeline = fileMap[file.RootName];

                    // Update [Audit].[ProcessFileTask] record
                    audit.updateProcessFileTaskRecord((long)processKey, (long)file.AuditProcessFileTaskKey, file.Pipeline, 'Y');
                }
                catch
                {
                    logger.log($"{process}: No pipeline listed for {file.Name}");
                    Console.WriteLine($"{process}: No pipeline listed for {file.Name}");

                    // Fail process record
                    processStatusFlag = 'N';

                    try
                    {

                        // Update [Audit].[ProcessFileTask] record
                        audit.updateProcessFileTaskRecord((long)processKey, (long)file.AuditProcessFileTaskKey, null, 'N');

                        // Write to [Audit].[ErrorLog]
                        audit.writeToErrorLog(processKey, null, null, null, process, processDescription, $"No pipeline listed for {file.Name}");
                    }
                    catch (Exception e)
                    {
                        logger.log(e.ToString());
                        Console.WriteLine(e.ToString());
                    }

                    try
                    {
                        // Send unrecognized file to error directory
                        archiveFileQueue.AddMessage(new CloudQueueMessage($"0;{file.Name};{file.RootName}"));
                    }
                    catch (Exception e)
                    {
                        logger.log($"{process} failed to archive file {file.Name}. {e}");
                        Console.WriteLine($"{process} failed to archive file {file.Name}. {e}");

                        try
                        {
                            // Send email alert
                            emailAlertQueue.AddMessage(new CloudQueueMessage($"{process};{process} failed to archive file {file.Name};{Convert.ToString(processKey)};{Convert.ToString(file.AuditProcessFileTaskKey)};;"));
                        }
                        catch (Exception ex)
                        {
                            logger.log(ex.ToString());
                            Console.WriteLine(ex.ToString());
                        }
                    }

                    try
                    {
                        // Send email alert
                        emailAlertQueue.AddMessage(new CloudQueueMessage($"{process};No pipeline listed for {file.Name};{Convert.ToString(processKey)};{Convert.ToString(file.AuditProcessFileTaskKey)};;"));
                    }
                    catch (Exception e)
                    {
                        logger.log(e.ToString());
                        Console.WriteLine(e.ToString());
                    }

                    // Flag file for exclusion from proceeding operations
                    file.SkipFlag = true;
                }
            } // End loop

            // Authenticate app and create a data factory management client
            DataFactoryManagementClient client = null;
            try
            {
                client = Helper.getDataFactoryClient(tenantID, applicationId, authenticationKey, subscriptionId);
            }
            catch (Exception e)
            {
                logger.log($"{process} failed to create an authenticated Data Factory Management Client. {e}");
                Console.WriteLine($"{process} failed to create an authenticated Data Factory Management Client. {e}");

                try
                {
                    // Update [Audit].[Process] record
                    audit.updateProcessRecord((long)processKey, 'N');
                }
                catch (Exception ex)
                {
                    logger.log(ex.ToString());
                    Console.WriteLine(ex.ToString());
                }

                try
                {
                    // Send email alert
                    emailAlertQueue.AddMessage(new CloudQueueMessage($"{process};{process} failed to create an authenticated Data Factory Management Client.;{processKey};;;"));
                }
                catch (Exception ex)
                {
                    logger.log(ex.ToString());
                    Console.WriteLine(ex.ToString());
                }

                // Stop the application
                throw new Exception();
            }

            // Load pipeline parameters
            List<ParameterObject> parameters = Helper.getParams(parametersAddress);

            // Execute Data Factory pipelines in order to prevent overwriting newer data
            foreach (File file in files.OrderBy(f => f.Name))
            {
                // Skip file if flagged
                if (file.SkipFlag)
                    continue;

                // Select pipeline parameters
                IDictionary<string, object> ADFParameters = new Dictionary<string, object>();
                try
                {
                    ADFParameters = Helper.selectParams(file.Pipeline, parameters);
                }
                catch
                {
                    logger.log($"No parameters listed for {file.Pipeline}");
                    Console.WriteLine($"No parameters listed for {file.Pipeline}");

                    // Fail process record
                    processStatusFlag = 'N';

                    try
                    {
                        // Write to [Audit].[ErrorLog]
                        audit.writeToErrorLog(processKey, null, null, null, process, processDescription, $"No parameters listed for {file.Pipeline}");
                    }
                    catch (Exception e)
                    {
                        logger.log(e.ToString());
                        Console.WriteLine(e.ToString());
                    }

                    try
                    {
                        // Send failed file to error directory
                        archiveFileQueue.AddMessage(new CloudQueueMessage($"0;{file.Name};{file.RootName}"));
                    }
                    catch (Exception e)
                    {
                        logger.log($"{process} failed to archive file {file.Name}. {e}");
                        Console.WriteLine($"{process} failed to archive file {file.Name}. {e}");

                        try
                        {
                            emailAlertQueue.AddMessage(new CloudQueueMessage($"{process};{process} failed to archive file {file.Name};{Convert.ToString(processKey)};{Convert.ToString(file.AuditProcessFileTaskKey)};;"));
                        }
                        catch (Exception ex)
                        {
                            logger.log(ex.ToString());
                            Console.WriteLine(ex.ToString());
                        }
                    }

                    try
                    {
                        // Send email alert
                        emailAlertQueue.AddMessage(new CloudQueueMessage($"{process};No parameters listed for {file.Pipeline};{Convert.ToString(processKey)};{Convert.ToString(file.AuditProcessFileTaskKey)};;"));
                    }
                    catch (Exception e)
                    {
                        logger.log(e.ToString());
                        Console.WriteLine(e.ToString());
                    }

                    // Skip to the next file
                    continue;
                }

                // Add file name and Process/ProcessFileTask Audit keys to parameters
                ADFParameters["ProcessSubtask1_FileName"] = file.Name;
                ADFParameters["ProcessSubtask1_FilePath"] = sftpcreds.remoteDirectory;
                ADFParameters["AuditProcessKey"] = processKey;
                ADFParameters["AuditProcessFileTaskKey"] = file.AuditProcessFileTaskKey;

                // Execute ADF pipeline
                CreateRunResponse runResponse = new CreateRunResponse();
                for (int tryCount = 1; tryCount < maxRetryCount + 1; tryCount++) // For loop for transient fault handling
                {
                    try
                    {
                        logger.log($"Executing ADF Pipeline {file.Pipeline} for {file.Name}...");
                        Console.WriteLine($"Executing ADF Pipeline: {file.Pipeline} for {file.Name}...");

                        // Pipeline execution
                        runResponse = client.Pipelines.CreateRunWithHttpMessagesAsync(resourceGroup, dataFactoryName, file.Pipeline, ADFParameters).Result.Body;

                        logger.log($"Pipeline execution successful.  RunId: {runResponse.RunId}");

                        break;
                    }
                    catch
                    {
                        if (tryCount < maxRetryCount)
                        {
                            logger.log($"{process}: Cannot connect to Data Factory pipeline: {file.Pipeline}. Retrying connection in {tryCount} minutes...");
                            Console.WriteLine($"{process}: Cannot connect to Data Factory pipeline: {file.Pipeline}. Retrying connection in {tryCount} minutes...");

                            // Wait trycount number of minutes
                            System.Threading.Thread.Sleep(tryCount * 60000);

                            // Renew Data Factory client
                            try
                            {
                                client = Helper.getDataFactoryClient(tenantID, applicationId, authenticationKey, subscriptionId);
                            }
                            catch (Exception e)
                            {
                                logger.log($"{process} failed to renew Data Factory Management Client. Retrying connection in {tryCount} minutes... {e}");
                                Console.WriteLine($"{process} failed to renew Data Factory Management Client. Retrying connection in {tryCount} minutes... {e}");
                            }
                        }
                        else
                        {
                            logger.log($"{process}: Failed to connect to Data Factory pipeline: {file.Pipeline}. Aborting operation");
                            Console.WriteLine($"{process}: Failed to connect to Data Factory pipeline: {file.Pipeline}. Aborting operation");

                            // Fail process record
                            processStatusFlag = 'N';

                            try
                            {
                                // Write to [Audit].[ErrorLog]
                                audit.writeToErrorLog(processKey, null, file.AuditProcessFileTaskKey, null, process, processDescription, $"Failed to connect to Data Factory pipeline: {file.Pipeline}");
                            }
                            catch (Exception e)
                            {
                                logger.log(e.ToString());
                                Console.WriteLine(e.ToString());
                            }

                            try
                            {
                                // Send email alert
                                emailAlertQueue.AddMessage(new CloudQueueMessage($"{process};Failed to connect to Data Factory pipeline: {file.Pipeline};{Convert.ToString(processKey)};{Convert.ToString(file.AuditProcessFileTaskKey)};;"));
                            }
                            catch (Exception e)
                            {
                                logger.log(e.ToString());
                                Console.WriteLine(e.ToString());
                            }

                            try
                            {
                                // Send failed file to error directory
                                archiveFileQueue.AddMessage(new CloudQueueMessage($"0;{file.Name};{file.RootName}"));
                            }
                            catch (Exception e)
                            {
                                logger.log($"{process} failed to archive file {file.Name}. {e}");
                                Console.WriteLine($"{process} failed to archive file {file.Name}. {e}");

                                try
                                {
                                    // Send email alert
                                    emailAlertQueue.AddMessage(new CloudQueueMessage($"{process};{process} failed to archive file {file.Name};{Convert.ToString(processKey)};{Convert.ToString(file.AuditProcessFileTaskKey)};;"));
                                }
                                catch (Exception ex)
                                {
                                    logger.log(ex.ToString());
                                    Console.WriteLine(ex.ToString());
                                }
                            }

                            // If flagged, skip to the next file
                            file.SkipFlag = true;
                        }
                    }
                }

                // If flagged, skip to the next file
                if (file.SkipFlag)
                    continue;

                // Monitor Pipeline run status
                PipelineRun run = null;
                while (true)
                {
                    for (int tryCount = 1; tryCount < maxRetryCount + 1; tryCount++) // For loop for transient fault handling
                    {
                        try
                        {
                            // Get pipeline status
                            run = client.PipelineRuns.Get(resourceGroup, dataFactoryName, runResponse.RunId);
                            break;
                        }
                        catch (Exception e)
                        {
                            if (tryCount < maxRetryCount)
                            {
                                logger.log($"Unable to retrieve {file.Pipeline} run status.  Trying again in 30 seconds...");
                                Console.WriteLine($"Unable to retrieve {file.Pipeline} run status.  Trying again in 30 seconds...");

                                // Wait 30 seconds
                                System.Threading.Thread.Sleep(30000);

                                // Renew Data Factory client
                                try
                                {
                                    client = Helper.getDataFactoryClient(tenantID, applicationId, authenticationKey, subscriptionId);
                                }
                                catch (Exception ex)
                                {
                                    logger.log($"{process} failed to renew Data Factory Management Client. Trying again in 30 seconds... {ex}");
                                    Console.WriteLine($"{process} failed to renew Data Factory Management Client. Trying again in 30 seconds... {ex}");
                                }
                            }
                            else
                            {
                                logger.log($"Unable to retrieve {file.Pipeline} run status. Operation was abandoned. {e}");
                                Console.WriteLine($"Unable to retrieve {file.Pipeline} run status. Operation was abandoned. {e}");

                                // Fail process record
                                processStatusFlag = 'N';

                                try
                                {
                                    // Write to [Audit].[ErrorLog]
                                    audit.writeToErrorLog(processKey, null, file.AuditProcessFileTaskKey, null, process, processDescription, $"Failed to retrieve {file.Pipeline} run status. {e.Message}");
                                }
                                catch (Exception ex)
                                {
                                    logger.log(ex.ToString());
                                    Console.WriteLine(ex.ToString());
                                }

                                try
                                {
                                    // Send email alert
                                    emailAlertQueue.AddMessage(new CloudQueueMessage($"{process};Unable to retrieve {file.Pipeline} run status;{Convert.ToString(processKey)};{Convert.ToString(file.AuditProcessFileTaskKey)};;"));
                                }
                                catch (Exception ex)
                                {
                                    logger.log(ex.ToString());
                                    Console.WriteLine(ex.ToString());
                                }

                                try
                                {
                                    // Send failed file to error directory
                                    archiveFileQueue.AddMessage(new CloudQueueMessage($"0;{file.Name};{file.RootName}"));
                                }
                                catch (Exception ex)
                                {
                                    logger.log($"{process} failed to archive file {file.Name}. {ex}");
                                    Console.WriteLine($"{process} failed to archive file {file.Name}. {ex}");

                                    try
                                    {
                                        // Send email alert
                                        emailAlertQueue.AddMessage(new CloudQueueMessage($"{process};{process} failed to archive file {file.Name};{Convert.ToString(processKey)};{Convert.ToString(file.AuditProcessFileTaskKey)};;"));
                                    }
                                    catch (Exception exc)
                                    {
                                        logger.log(exc.ToString());
                                        Console.WriteLine(exc.ToString());
                                    }
                                }

                                // Flag file
                                file.SkipFlag = true;
                            }                            
                        }
                    }

                    // If flagged, skip to the next file
                    if (file.SkipFlag)
                        break;

                    Console.WriteLine("Status: " + run.Status);

                    if (run.Status == "InProgress")     // Waiting to complete
                    {
                        // Wait 15 seconds
                        System.Threading.Thread.Sleep(15000);
                    }
                    else if (run.Status == "Succeeded")     // Completed successfully
                    {                        
                        logger.log($"RunId: {runResponse.RunId} - Pipeline {file.Pipeline} successfully completed");
                        Console.WriteLine($"RunId: {runResponse.RunId} - Pipeline {file.Pipeline} successfully completed");

                        // Collect results of the copy activity                        
                        List<ActivityRun> activities = client.ActivityRuns.ListByPipelineRun(
                            resourceGroup, dataFactoryName, run.RunId, DateTime.UtcNow.AddMinutes(-15), DateTime.UtcNow.AddMinutes(+15)).ToList();

                        ActivityRun activity = activities.Where(a => a.ActivityName == "CopyActivity").First();

                        CopyActivityOutput output = JsonConvert.DeserializeObject<CopyActivityOutput>(activity.Output.ToString());

                        // If the copy activity found and skipped incompatible rows, fail the process.
                        if (output.RowsSkipped > 0)
                            processStatusFlag = 'N';

                        // If the data is sourced from CustomersFeed or OrdersFeed, activate the parsing functions
                        if (file.RootName == orders)
                        {
                            using (SqlConnection connection = new SqlConnection(sqlConnectionString))
                            {                                                              
                                OrdersParser order = new OrdersParser(connection, audit, logger, emailAlertQueue, (long)processKey);
                                char statusFlag1 = order.ExtractOrders();
                                char statusFlag2 = order.ExtractOrderLineItems();
                                char statusFlag3 = order.ExtractOrderPayments();

                                // If any of the parsing functions return a failed flag:
                                if (statusFlag1 == 'N' || statusFlag2 == 'N' || statusFlag3 == 'N')
                                {
                                    // Fail the process record
                                    processStatusFlag = 'N';

                                    try
                                    {
                                        // Archive failed OrdersFeed file
                                        archiveFileQueue.AddMessage(new CloudQueueMessage($"0;{file.Name};{file.RootName}"));
                                    }
                                    catch (Exception e)
                                    {
                                        logger.log($"{process} failed to archive file {file.Name}. {e}");
                                        Console.WriteLine($"{process} failed to archive file {file.Name}. {e}");

                                        try
                                        {
                                            // Send email alert
                                            emailAlertQueue.AddMessage(new CloudQueueMessage($"{process};{process} failed to archive file {file.Name};{processKey};{file.AuditProcessFileTaskKey};;"));
                                        }
                                        catch (Exception ex)
                                        {
                                            logger.log(ex.ToString());
                                            Console.WriteLine(ex.ToString());
                                        }
                                    }
                                }
                                else
                                {
                                    try
                                    {
                                        // Archive successfully integrated file
                                        archiveFileQueue.AddMessage(new CloudQueueMessage($"1;{file.Name};{file.RootName}"));
                                    }
                                    catch (Exception e)
                                    {
                                        logger.log($"{process} failed to archive file {file.Name}. {e}");
                                        Console.WriteLine($"{process} failed to archive file {file.Name}. {e}");

                                        try
                                        {
                                            // Send email alert
                                            emailAlertQueue.AddMessage(new CloudQueueMessage($"{process};{process} failed to archive file {file.Name};{processKey};{file.AuditProcessFileTaskKey};;"));
                                        }
                                        catch (Exception ex)
                                        {
                                            logger.log(ex.ToString());
                                            Console.WriteLine(ex.ToString());
                                        }
                                    }
                                }
                            }
                        }
                        else if (file.RootName == customers)
                        {
                            using (SqlConnection connection = new SqlConnection(sqlConnectionString))
                            {
                                CustomersParser customer = new CustomersParser(connection, audit, logger, emailAlertQueue, (long)processKey);
                                char statusFlag1 = customer.ExtractCustomers();
                                char statusFlag2 = customer.ExtractCustomerGroups();
                                char statusFlag3 = customer.ExtractLoyaltyTier();

                                // Fail the process if any of the parsing functions return a failed flag
                                if (statusFlag1 == 'N' || statusFlag2 == 'N' || statusFlag3 == 'N')
                                {
                                    // Fail the process record
                                    processStatusFlag = 'N';

                                    try
                                    {
                                        // Archive failed customersFeed file
                                        archiveFileQueue.AddMessage(new CloudQueueMessage($"0;{file.Name};{file.RootName}"));
                                    }
                                    catch (Exception e)
                                    {
                                        logger.log($"{process} failed to archive file {file.Name}. {e}");
                                        Console.WriteLine($"{process} failed to archive file {file.Name}. {e}");

                                        try
                                        {
                                            // Send email alert
                                            emailAlertQueue.AddMessage(new CloudQueueMessage($"{process};{process} failed to archive file {file.Name};{processKey};{file.AuditProcessFileTaskKey};;"));
                                        }
                                        catch (Exception ex)
                                        {
                                            logger.log(ex.ToString());
                                            Console.WriteLine(ex.ToString());
                                        }
                                    }
                                }
                                else
                                {
                                    try
                                    {
                                        // Archive successfully integrated file
                                        archiveFileQueue.AddMessage(new CloudQueueMessage($"1;{file.Name};{file.RootName}"));
                                    }
                                    catch (Exception e)
                                    {
                                        logger.log($"{process} failed to archive file {file.Name}. {e}");
                                        Console.WriteLine($"{process} failed to archive file {file.Name}. {e}");

                                        try
                                        {
                                            // Send email alert
                                            emailAlertQueue.AddMessage(new CloudQueueMessage($"{process};{process} failed to archive file {file.Name};{processKey};{file.AuditProcessFileTaskKey};;"));
                                        }
                                        catch (Exception ex)
                                        {
                                            logger.log(ex.ToString());
                                            Console.WriteLine(ex.ToString());
                                        }
                                    }
                                }

                            }
                        }
                        else
                        {
                            try
                            {
                                // Archive successfully integrated file
                                archiveFileQueue.AddMessage(new CloudQueueMessage($"1;{file.Name};{file.RootName}"));
                            }
                            catch (Exception e)
                            {
                                logger.log($"{process} failed to archive file {file.Name}. {e}");
                                Console.WriteLine($"{process} failed to archive file {file.Name}. {e}");

                                try
                                {
                                    // Send email alert
                                    emailAlertQueue.AddMessage(new CloudQueueMessage($"{process};{process} failed to archive file {file.Name};{processKey};{file.AuditProcessFileTaskKey};;"));
                                }
                                catch (Exception ex)
                                {
                                    logger.log(ex.ToString());
                                    Console.WriteLine(ex.ToString());
                                }
                            }
                        }

                        // Exit while loop at the end of successful file integration activities
                        break;
                    }
                    else    // Failed activity
                    {
                        // locate error
                        string error = string.Empty;
                        string activityName = string.Empty;

                        // Fail process record
                        processStatusFlag = 'N';

                        try
                        {
                            // Get list of pipeline activities
                            List<ActivityRun> activities = client.ActivityRuns.ListByPipelineRun(
                                resourceGroup, dataFactoryName, run.RunId, DateTime.UtcNow.AddMinutes(-15), DateTime.UtcNow.AddMinutes(+15)).ToList();

                            // Select failed activity and error
                            ActivityRun activity = activities.Where(a => a.Status == "Failed").First();
                            activityName = activity.ActivityName;
                            error = activity.Error.ToString();
                        }
                        catch (Exception e)
                        {
                            logger.log($"{process} was unable to identify data factory error for {file.Pipeline}. {e}");
                            Console.WriteLine($"{process} was unable to identify data factory error for {file.Pipeline}. {e}");
                        }

                        logger.log($"RunId: {runResponse.RunId} - {file.Pipeline} failed.  Failed Activity: {activityName}.  Error Message: {error}");
                        Console.WriteLine($"RunId: {runResponse.RunId} - {file.Pipeline} failed.  Failed Activity: {activityName}.  Error Message: {error}");

                        try
                        {
                            // Add record to [Audit].[ErrorLog]
                            audit.writeToErrorLog(processKey, null, file.AuditProcessFileTaskKey, null, process, processDescription,
                                                    $"{file.Pipeline} - {activityName} failed: {error}", null, null);
                        }
                        catch (Exception e)
                        {
                            logger.log(e.ToString());
                            Console.WriteLine(e.ToString());
                        }

                        try
                        {
                            // Send email alert
                            emailAlertQueue.AddMessage(new CloudQueueMessage($"{process};{file.Pipeline} - {activityName} failed: {error};{Convert.ToString(processKey)};{Convert.ToString(file.AuditProcessFileTaskKey)};;"));
                        }
                        catch (Exception e)
                        {
                            logger.log(e.ToString());
                            Console.WriteLine(e.ToString());
                        }

                        try
                        {
                            // Send file to error directory
                            archiveFileQueue.AddMessage(new CloudQueueMessage($"0;{file.Name};{file.RootName}"));
                        }
                        catch (Exception e)
                        {
                            logger.log($"{process} failed to archive file {file.Name}. {e}");
                            Console.WriteLine($"{process} failed to archive file {file.Name}. {e}");

                            try
                            {
                                // Send email alert
                                emailAlertQueue.AddMessage(new CloudQueueMessage($"{process};{process} failed to archive file {file.Name};{processKey};{file.AuditProcessFileTaskKey};;"));
                            }
                            catch (Exception ex)
                            {
                                logger.log(ex.ToString());
                                Console.WriteLine(ex.ToString());
                            }
                        }                       

                        break;
                    }
                }                
            } // End loop

            // Mark the process as successful if no failures occurred
            if (processStatusFlag != 'N')
                processStatusFlag = 'Y';

            try
            {
                // Update [Audit].[Process] record
                audit.updateProcessRecord((long)processKey, (char)processStatusFlag);
            }
            catch (Exception e)
            {
                logger.log($"{process} failed to update process record. {e}");
                Console.WriteLine($"{process} failed to update process record. {e}");

                try
                {
                    // Send email alert
                    emailAlertQueue.AddMessage(new CloudQueueMessage($"{process};{process} failed to update process record;{processKey};;;"));
                }
                catch (Exception ex)
                {
                    logger.log(ex.ToString());
                    Console.WriteLine(ex.ToString());
                }
            }

            // Trigger CRM Integration Pipeline
            if (executeCRM)
                TriggerCRMIntegration(logger, emailAlertQueue);
        }

        public static void TriggerCRMIntegration(Logger logger, CloudQueue emailAlertQueue)
        {
            // Constants
            string pipelineName = "Pipeline_MasterPackage";

            // Parameters File
            string parametersFileName = ConfigurationManager.AppSettings["CRMParametersFile"].ToString();

            // Azure AD app credentials    
            string tenantID = ConfigurationManager.AppSettings["TenantID"].ToString();
            string applicationId = ConfigurationManager.AppSettings["ApplicationID"].ToString();
            string authenticationKey = ConfigurationManager.AppSettings["AuthenticationKey"].ToString();
            string subscriptionId = ConfigurationManager.AppSettings["SubscriptionID"].ToString();
            string resourceGroup = ConfigurationManager.AppSettings["ResourceGroup"].ToString();
            string dataFactoryName = ConfigurationManager.AppSettings["DataFactoryName_CRM"].ToString();

            // Max retries for transient errors
            int maxRetryCount = Convert.ToInt32(ConfigurationManager.AppSettings["MaxRetryCount"].ToString());

            // Create parameters object
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters["FileName"] = parametersFileName;

            // Authenticate app and create a data factory management client
            DataFactoryManagementClient client = null;
            try
            {
                client = Helper.getDataFactoryClient(tenantID, applicationId, authenticationKey, subscriptionId);
            }
            catch (Exception e)
            {
                logger.log($"TriggerCRMIntegration - SFCC failed to create an authenticated Data Factory Management Client. {e}");
                Console.WriteLine($"TriggerCRMIntegration - SFCC failed to create an authenticated Data Factory Management Client. {e}");

                try
                {
                    // Send email alert
                    emailAlertQueue.AddMessage(new CloudQueueMessage($"TriggerCRMIntegration - SFCC;TriggerCRMIntegration - SFCC failed to create an authenticated Data Factory Management Client.;;;;"));
                }
                catch (Exception ex)
                {
                    logger.log(ex.ToString());
                    Console.WriteLine(ex.ToString());
                }

                // Stop the application
                throw new Exception();
            }

            // Trigger CRM integration
            CreateRunResponse runResponse = new CreateRunResponse();
            for (int tryCount = 1; tryCount < maxRetryCount + 1; tryCount++) // For loop for transient fault handling
            {
                try
                {
                    logger.log($"Executing CRM Integration - SFCC...");
                    Console.WriteLine($"Executing CRM Integration - SFCC...");

                    // Pipeline execution
                    runResponse = client.Pipelines.CreateRunWithHttpMessagesAsync(resourceGroup, dataFactoryName, pipelineName, parameters).Result.Body;

                    logger.log($"Pipeline execution successful");

                    break;
                }
                catch
                {
                    if (tryCount < maxRetryCount)
                    {
                        logger.log($"CRM Integration - SFCC failed. Retrying connection in {tryCount} minutes...");
                        Console.WriteLine($"CRM Integration - SFCC failed. Retrying connection in {tryCount} minutes...");

                        // Wait trycount number of minutes
                        System.Threading.Thread.Sleep(tryCount * 60000);

                        // Renew Data Factory client
                        try
                        {
                            client = Helper.getDataFactoryClient(tenantID, applicationId, authenticationKey, subscriptionId);
                        }
                        catch (Exception e)
                        {
                            logger.log($"Failed to renew Data Factory Management Client. Retrying connection in {tryCount} minutes... {e}");
                            Console.WriteLine($"Failed to renew Data Factory Management Client. Retrying connection in {tryCount} minutes... {e}");
                        }
                    }
                    else
                    {
                        logger.log($"Failed to connect to CRM Integration - SFCC pipeline. Aborting operation");
                        Console.WriteLine($"Failed to connect to CRM Integration - SFCC pipeline. Aborting operation");

                        try
                        {
                            // Send email alert
                            emailAlertQueue.AddMessage(new CloudQueueMessage($"CRM Integration - SFCC;Failed to connect to CRM Integration - SFCC pipeline;;;;"));
                        }
                        catch (Exception e)
                        {
                            logger.log(e.ToString());
                            Console.WriteLine(e.ToString());
                        }
                    }
                }
            }
        }
    }
}
