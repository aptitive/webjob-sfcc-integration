﻿using Newtonsoft.Json;

namespace webjob_sfcc_integration_prod1
{
    public class LoyaltyTier
    {
        public string CustomerID { get; set; }
        public int LoyaltyTierID { get; set; }
        public string Name { get; set; }
    }

    public class LoyaltyTierJson
    {
        [JsonProperty("id")]
        public int id { get; set; }

        [JsonProperty("name")]
        public string name { get; set; }

        [JsonProperty("image_url")]
        public string image_url { get; set; }
    }
}
