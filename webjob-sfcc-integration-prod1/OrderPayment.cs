﻿using Newtonsoft.Json;

namespace webjob_sfcc_integration_prod1
{
    public class OrderPayment
    {
        [JsonProperty("PaymentId")]
        public string PaymentId { get; set; }

        [JsonProperty("OrderNo")]
        public string OrderNo { get; set; }

        [JsonProperty("CCLast4")]
        public string CCLast4 { get; set; }

        [JsonProperty("CCType")]
        public string CCType { get; set; }

        [JsonProperty("PaymentMethod")]
        public string PaymentMethod { get; set; }

        [JsonProperty("PaymentSubtotal")]
        public string PaymentSubtotal { get; set; }

        [JsonProperty("BillingFirstName")]
        public string BillingFirstName { get; set; }

        [JsonProperty("BillingLastName")]
        public string BillingLastName { get; set; }

        [JsonProperty("BillingAddress")]
        public string BillingAddress { get; set; }

        [JsonProperty("BillingAddress2")]
        public string BillingAddress2 { get; set; }

        [JsonProperty("BillingAddressCity")]
        public string BillingAddressCity { get; set; }

        [JsonProperty("BillingAddressState")]
        public string BillingAddressState { get; set; }

        [JsonProperty("BillingAddressPostalCode")]
        public string BillingAddressPostalCode { get; set; }

        [JsonProperty("BillingAddressCountry")]
        public string BillingAddressCountry { get; set; }

        [JsonProperty("BillingAddressPhone")]
        public string BillingAddressPhone { get; set; }
    }
}
