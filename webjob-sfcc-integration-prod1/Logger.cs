﻿using System;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace webjob_sfcc_integration_prod1
{
    public class Logger
    {
        private CloudAppendBlob appendBlob;

        public Logger(string fileName, string connstr, string container)
        {
            CloudStorageAccount cloudStorageAccount = CloudStorageAccount.Parse(connstr);
            CloudBlobClient cloudBlobClient = cloudStorageAccount.CreateCloudBlobClient();
            CloudBlobContainer cloudBlobContainer = cloudBlobClient.GetContainerReference(container);

            // Get log file reference
            appendBlob = cloudBlobContainer.GetAppendBlobReference($"{fileName}-{DateTime.Today.ToString("MMddyyyy")}.log");

            // Create file if it does not exist
            if (!appendBlob.Exists()) appendBlob.CreateOrReplace();
        }

        /// <summary>
        /// Log text to Azure append blob
        /// </summary>
        /// <param name="text"></param>
        public void log(string text)
        {
            // Add text to log file
            appendBlob.AppendTextAsync($"{DateTime.Now}: {text}\n");
        }
    }
}
