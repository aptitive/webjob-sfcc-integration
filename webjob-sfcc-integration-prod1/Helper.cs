﻿using System.Linq;
using System.Collections.Generic;
using Microsoft.Rest;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.File;
using Newtonsoft.Json;
using SshNet = Renci.SshNet;
using Renci.SshNet.Sftp;
using System.IO;
using System;
using Microsoft.Azure.Management.DataFactory;

namespace webjob_sfcc_integration_prod1
{
    public class Helper
    {
        /// <summary>
        /// Retrieves file names and metadata from SFTP server
        /// </summary>
        /// <param name="host"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="remoteDirectory"></param>
        /// <param name="fileType"></param>
        /// <returns>Returns File list</returns>
        public static List<File> getSFTPFiles(SftpCredentials sftpcreds, string fileType)
        {
            List<File> files = new List<File>();
            using (var sftp = new SshNet.SftpClient(sftpcreds.host, sftpcreds.username, sftpcreds.password))
            {
                // Open connection to sftp
                sftp.Connect(); 

                // List files in remote directory
                IEnumerable<SftpFile> rawFiles = sftp.ListDirectory(sftpcreds.remoteDirectory);

                // Collect file names and metadata
                foreach (var rawFile in rawFiles)
                {
                    string filePath = rawFile.FullName;
                    string fileExtension = filePath.Substring(filePath.Length - 3, 3);
                    string fileName = filePath.Replace("/" + sftpcreds.remoteDirectory, "");

                    if (fileExtension == fileType) // add only files of fileType to file list
                    {
                        File file = new File()
                        {
                            Name = fileName,
                            RootName = fileName.Split('_')[0],
                            Size = Convert.ToString(sftp.GetAttributes(filePath).Size),
                            Type = fileExtension,
                            Location = "https://" + sftpcreds.host + filePath,
                            SkipFlag = false
                        };
                        files.Add(file);
                    }
                }

                // Close connection
                sftp.Disconnect(); 
            }
            return files;
        }

        /// <summary>
        /// Retrieves file names and metadata from Azure File Storage
        /// </summary>
        /// <param name="connstr"></param>
        /// <param name="fileshare"></param>
        /// <returns>Returns File list</returns>
        public static List<File> getAZFiles(string connstr, string fileshare)
        {
            // Create a storage account object
            CloudStorageAccount account = CloudStorageAccount.Parse(connstr);

            // Get root directory reference
            CloudFileDirectory dir = account.CreateCloudFileClient()
                                            .GetShareReference(fileshare)
                                            .GetRootDirectoryReference();

            // Retrieve a list of files in the fileshare
            IEnumerable<IListFileItem> fileList = dir.ListFilesAndDirectories().OfType<CloudFile>();

            // Create file list
            List<File> files = new List<File>();
            foreach (IListFileItem fileItem in fileList)
            {
                // Get CloudFile reference
                CloudFile fileReference = new CloudFile(fileItem.Uri);
                CloudFile fileObject = dir.GetFileReference(fileReference.Name);

                // Populate CloudFile metadata
                fileObject.FetchAttributes();

                // Add new file object to list
                files.Add(new File
                {
                    Name = fileObject.Name,
                    RootName = fileObject.Name.Split('_')[0],
                    Size = Convert.ToString(fileObject.Properties.Length),
                    Type = fileObject.Name.Split('.')[1],
                    Location = Convert.ToString(fileItem.Uri),
                    SkipFlag = false
                });
            }
            return files;
        }

        /// <summary>
        /// Retrieves and converts the csv files listing expected columns configurations to a string array
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static string[] GetColumnsList(string filePath)
        {
            using (StreamReader rdr = new StreamReader(filePath))
            {
                return rdr.ReadToEnd()
                          .Split(',');
            }
        }

        /// <summary>
        /// Selects and validates the file schema of files stored in an Sftp server
        /// </summary>
        /// <param name="columnsCatalog"></param>
        /// <param name="fileCategory"></param>
        /// <param name="sftpcreds"></param>
        public static string ValidateFileSchema(File file, Dictionary<string, string[]> columnsCatalog, SftpCredentials sftpcreds)
        {
            using (var sftp = new SshNet.SftpClient(sftpcreds.host, sftpcreds.username, sftpcreds.password))
            {
                // Open connection to sftp
                sftp.Connect();

                // Split the first line of the file into a list of columns
                List<string> fileColumns = new List<string>();
                using (StreamReader reader = new StreamReader(sftp.OpenRead(file.Location.Replace("https://" + sftpcreds.host, ""))))
                {
                    string firstLine = reader.ReadLine() ?? "";
                    fileColumns = firstLine.Split('|').ToList();
                }

                string[] columns = { };
                try
                {
                    // Select columns list
                    columns = columnsCatalog[file.RootName];
                }
                catch
                {
                    return $"{file.Name} is not an expected file name.";
                }

                // Validate file schema
                if (columns.Count() == fileColumns.Count())
                {
                    for (int i = 0; i < fileColumns.Count(); i++)
                    {
                        if (columns[i] != fileColumns[i].Trim())
                            return $"{file.Name} does not match expected schema. Expected column: {columns[i]}  Received column: {fileColumns[i]}";
                    }
                }
                else
                {
                    return $"{file.Name} does not match expected schema. Expected column count: {columns.Count()}  Actual column count: {fileColumns.Count()}";
                }

                return "";
            }
        }

        /// <summary>
        /// Selects potentially conflicting files from file list
        /// </summary>
        /// <param name="files"></param>
        /// <returns>Returns File list</returns>
        public static List<File> filterConflicts(List<File> files)
        {
            List<File> conflicts = new List<File>();
            foreach (File file in files)
            {
                string name = file.RootName;
                
                // If more than two files in the list are derived from the same source, add to conflicts list
                if (files.Where(f => f.RootName == name).Count() > 1)
                {
                    conflicts.Add(file);
                }
            }
            return conflicts;
        }

        /// <summary>
        /// Deserializes file map json into key/value pairs
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        public static Dictionary<string, string> loadFileMap(string address)
        {
            string strFileMap = "";
            using (StreamReader r = new StreamReader(address))
            {
                strFileMap += r.ReadToEnd();
            }
            return JsonConvert.DeserializeObject<Dictionary<string, string>>(strFileMap);
        }

        /// <summary>
        /// Retrieves service client credentials from Azure AD
        /// </summary>
        /// <param name="tenantID"></param>
        /// <param name="applicationId"></param>
        /// <param name="authenticationKey"></param>
        /// <returns></returns>
        public static ServiceClientCredentials getCredentials(string tenantID, string applicationId, string authenticationKey)
        {
            var context = new AuthenticationContext("https://login.windows.net/" + tenantID);
            ClientCredential cc = new ClientCredential(applicationId, authenticationKey);
            AuthenticationResult result = context.AcquireTokenAsync("https://management.azure.com/", cc).Result;
            ServiceClientCredentials cred = new TokenCredentials(result.AccessToken);
            return cred;
        }

        /// <summary>
        /// Creates a DataFactoryManagementClient
        /// </summary>
        /// <param name="tenantID"></param>
        /// <param name="applicationId"></param>
        /// <param name="authenticationKey"></param>
        /// <param name="subscriptionId"></param>
        /// <returns></returns>
        public static DataFactoryManagementClient getDataFactoryClient(string tenantID, string applicationId, string authenticationKey, string subscriptionId)
        {
            var cred = getCredentials(tenantID, applicationId, authenticationKey);
            DataFactoryManagementClient client = new DataFactoryManagementClient(cred) { SubscriptionId = subscriptionId };
            return client;
        }

        /// <summary>
        /// Deserializes Data Factory parameters json file into a list of Data Factory parameters objects
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        public static List<ParameterObject> getParams(string address)
        {
            var json = "";
            using (StreamReader r = new StreamReader(address))
            {
                json = r.ReadToEnd();
            }
            return JsonConvert.DeserializeObject<List<ParameterObject>>(json);
        }

        /// <summary>
        /// Selects the appropriate Data Factory parameters object from the parameters list
        /// </summary>
        /// <param name="pipeline"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static Dictionary<string, object> selectParams(string pipeline, List<ParameterObject> parameters)
        {
            return parameters.Where(p => p.Name == pipeline)
                             .First()
                             .Parameters;
        }
    }
}
