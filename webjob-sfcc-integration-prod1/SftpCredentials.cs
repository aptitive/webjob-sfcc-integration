﻿namespace webjob_sfcc_integration_prod1
{
    public class SftpCredentials
    {
        public string host { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string remoteDirectory { get; set; }
    }
}
