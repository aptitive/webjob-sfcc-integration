﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;

namespace webjob_sfcc_integration_prod1
{
    public class OrdersParser
    {
        //=========================================================== 
        //                  CONSTANTS
        //===========================================================

        // Parsing functions table variables
        const string sourceSchema = "Stage";
        const string sourceTable = "SFCC_OrdersFile";
        const string ordersSchema = "Stage";
        const string ordersTable = "SFCC_Orders";
        const string orderLineItemsSchema = "Stage";
        const string orderLineItemsTable = "SFCC_OrderLineItems";
        const string orderLineItemsColumnName = "Order_LineItems";
        const string orderPaymentsSchema = "Stage";
        const string orderPaymentsTable = "SFCC_OrderPayments";
        const string orderPaymentsColumnName = "Order_Payments";
        const string CDCColumnName = "UpdatedDate";

        // Parsing functions stored procedures
        const string SPExtractOrders = "[ETL].[Get_SFCC_Orders]";
        const string SPMergeOrders = "[ETL].[Merge_SFCC_Orders]";
        const string SPMergeOrderLineItems = "[ETL].[Merge_SFCC_OrderLineItems]";
        const string SPMergeOrderPayments = "[ETL].[Merge_SFCC_OrderPayments]";

        // Stored procedure timeout value in seconds
        const int timeout = 300;

        // SqlBulkCopy batch size
        const int batchsize = 10000;

        //===========================================================


        private SqlConnection conn;
        private Audit audit;
        private Logger logger;
        private CloudQueue emailAlertQueue;
        private long processKey;
        
        public OrdersParser(SqlConnection _conn, Audit _audit, Logger _logger, CloudQueue _emailAlertQueue, long _processKey)
        {
            this.conn = _conn;
            this.audit = _audit;
            this.logger = _logger;
            this.emailAlertQueue = _emailAlertQueue;
            this.processKey = _processKey;
        }

        public char ExtractOrders()
        {
            // Initialize ProcessTask key
            long? processTaskKey = null;
            try
            {
                // Insert new record into [Audit].[ProcessTask] table
                processTaskKey = audit.getInitialProcessTaskRecord(processKey, $"Extract Orders data from {sourceSchema}.{sourceTable} into SFCC.Orders");
            }
            catch (Exception e)
            {
                logger.log($"Failed to initialize ProcessTask record for ExtractOrders parsing function.");
                Console.WriteLine($"Failed to initialize ProcessTask record for ExtractOrders parsing function.");

                // Write to [Audit].[ErrorLog] table
                audit.writeToErrorLog(processKey, processTaskKey, null, null, "Azure WebJob - SFCC Integration: ExtractOrders",
                                        $"Extract Orders from Stage.SFCC_OrdersFile into {ordersSchema}.{ordersTable}", e.Message);

                // send email alert
                emailAlertQueue.AddMessage(new CloudQueueMessage($"Azure WebJob - SFCC Integration: ExtractOrders;{e};{Convert.ToString(processKey)};;;"));

                // Return fail flag
                return 'N';
            }

            // Initialize ProcessSubtaskKey
            long? processSubtaskKey = null;
 
            try
            {
                try
                {
                    logger.log($"Extracting Orders data from {sourceSchema}.{sourceTable}...");
                    Console.WriteLine($"Extracting Orders data from {sourceSchema}.{sourceTable}...");

                    // Insert new record into [Audit].[ProcessSubtask] table
                    processSubtaskKey = audit.getInitialProcessSubtaskRecord(processKey, (long)processTaskKey, $"Extract Orders from {sourceSchema}.{sourceTable} into {ordersSchema}.{ordersTable}",
                                                                                ordersSchema, ordersTable, true, sourceSchema, sourceTable, CDCColumnName, false);

                    // Open SQL connection
                    this.conn.Open();

                    // Extract orders data from [Stage].[SFCC_OrdersFile] into [Stage].[SFCC_Orders]                
                    using (SqlCommand cmd = new SqlCommand(SPExtractOrders, conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add(new SqlParameter("@AuditProcessKey", processKey));
                        cmd.Parameters.Add(new SqlParameter("@AuditProcessTaskKey", processTaskKey));
                        cmd.Parameters.Add(new SqlParameter("@AuditProcessSubtaskKey", processSubtaskKey));

                        cmd.ExecuteNonQuery();
                    }

                    // Close SQL connection
                    this.conn.Close();

                    logger.log($"Successfully extracted Orders columns into {ordersSchema}.{ordersTable}");
                    Console.WriteLine($"Successfully extracted Orders columns into {ordersSchema}.{ordersTable}");
                }
                catch (Exception e)
                {
                    logger.log($"Orders extraction failed. Details: {e}");
                    Console.WriteLine($"Orders extraction failed. Details: {e}");
                    throw new Exception($"Orders extraction failed. {e}");
                }

                try
                {
                    logger.log($"Collecting transaction metadata...");
                    Console.WriteLine($"Collecting transaction metadata...");

                    // Retrieve transaction metadata
                    int? sourceRowCount = null;
                    int? insertedRowCount = null;
                    int? finalTargetRowCount = null;

                    // Open SQL connection
                    this.conn.Open();

                    using (SqlCommand cmd = new SqlCommand($"SELECT COUNT(1) FROM [{sourceSchema}].[{sourceTable}]", conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                sourceRowCount = Convert.ToInt32(reader[0].ToString());
                                insertedRowCount = sourceRowCount; // All source rows are inserts
                            }
                        }
                    }

                    using (SqlCommand cmd = new SqlCommand($"SELECT COUNT(1) FROM [{ordersSchema}].[{ordersTable}];", conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                finalTargetRowCount = Convert.ToInt32(reader[0].ToString());
                            }
                        }
                    }

                    // Close SQL connection
                    this.conn.Close();

                    logger.log($"Successfully collected transaction metadata");
                    Console.WriteLine($"Successfully collected transaction metadata");

                    // Update [Audit].[ProcessSubtask] table
                    audit.updateProcessSubtaskRecord((long)processSubtaskKey, 'Y', false, sourceRowCount, 0, (int)finalTargetRowCount, insertedRowCount, 0);
                }
                catch (Exception e)
                {
                    logger.log($"Transaction metadata collection failed. Details: {e}");
                    Console.WriteLine($"Transaction metadata collection failed. Details: {e}");
                    throw new Exception($"Transaction metadata collection failed. {e}");
                }
            }
            catch (Exception e)
            {
                logger.log($"ExtractOrders failed to execute successfully. Error: {e}");
                Console.WriteLine($"ExtractOrders failed to execute successfully. Error: {e}");

                // Update [Audit].[ProcessSubtask] table
                audit.updateProcessSubtaskRecord((long)processSubtaskKey, 'N', false);

                // update [Audit].[ProcessTask] table
                audit.updateProcessTaskRecord((long)processTaskKey, 'N');

                // Write to [Audit].[ErrorLog] table
                audit.writeToErrorLog(processKey, processTaskKey, null, processSubtaskKey, "Azure WebJob - SFCC Integration: ExtractOrders",
                                        $"Extract Orders from Stage.SFCC_OrdersFile into {ordersSchema}.{ordersTable}", e.Message);

                // send email
                emailAlertQueue.AddMessage(new CloudQueueMessage($"Azure WebJob - SFCC Integration: ExtractOrders;{e};{Convert.ToString(processKey)};;{processTaskKey};{processSubtaskKey}"));

                // Return fail flag
                return 'N';
            }
            finally
            {
                // Close SQL connection
                this.conn.Close();
            }

            try
            {
                logger.log($"Executing ETL.Merge_SFCC_Orders...");
                Console.WriteLine($"Executing ETL.Merge_SFCC_Orders...");

                // Insert new record into [Audit].[ProcessSubtask] table
                processSubtaskKey = audit.getInitialProcessSubtaskRecord(processKey, (long)processTaskKey, $"Merge {ordersSchema}.{ordersTable} into SFCC.Orders",
                                                                            "SFCC", "Orders", true, ordersSchema, ordersTable, CDCColumnName, false);

                // Open SQL connection
                this.conn.Open();

                using (SqlCommand cmd = new SqlCommand(SPMergeOrders, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@AuditProcessSubtaskKey", processSubtaskKey));
                    cmd.Parameters.Add(new SqlParameter("@SourceHasAuditColumnsFlag", false));

                    cmd.ExecuteNonQuery();
                }

                logger.log($"Merge execution successful");
                Console.WriteLine($"Merge execution successful");

                // Close SQL connection
                this.conn.Close();

                // Update [Audit].[ProcessTask] table
                audit.updateProcessTaskRecord((long)processTaskKey, 'Y');

                logger.log($"ExtractOrders completed successfully");
                Console.WriteLine($"ExtractOrders completed successfully");

                // Return success flag
                return 'Y';
            }
            catch (Exception e)
            {
                // log exception
                logger.log($"Merge execution failed. Error: {e}");
                Console.WriteLine($"Merge execution failed. Error: {e}");

                // Update [Audit].[ProcessSubtask] table
                audit.updateProcessSubtaskRecord((long)processSubtaskKey, 'N', false);

                // update [Audit].[ProcessTask] table
                audit.updateProcessTaskRecord((long)processTaskKey, 'N');

                // Write to [Audit].[ErrorLog] table
                audit.writeToErrorLog(processKey, processTaskKey, null, processSubtaskKey, "Azure WebJob - SFCC Integration: ExtractOrders",
                                        $"Extract Orders from {sourceSchema}.{sourceTable} into SFCC.Orders", e.Message);

                // send email
                emailAlertQueue.AddMessage(new CloudQueueMessage($"Azure WebJob - SFCC Integration: ExtractOrders;{e};{Convert.ToString(processKey)};;{processTaskKey};{processSubtaskKey}"));

                // Return fail flag
                return 'N';
            }
            finally
            {
                // Close SQL connection
                this.conn.Close();
            }
        }

        public char ExtractOrderLineItems()
        {
            // Initialize ProcessTask key
            long? processTaskKey = null;
            try
            {
                // Insert new record into [Audit].[ProcessTask] table
                processTaskKey = audit.getInitialProcessTaskRecord(processKey, $"Extract OrderLineItems from {sourceSchema}.{sourceTable} into SFCC.OrderLineItems");
            }
            catch (Exception e)
            {
                logger.log($"Failed to initialize ProcessTask record for ExtractOrderLineItems parsing function.");
                Console.WriteLine($"Failed to initialize ProcessTask record for ExtractOrderLineItems parsing function.");

                // Write to [Audit].[ErrorLog] table
                audit.writeToErrorLog(processKey, processTaskKey, null, null, "Azure WebJob - SFCC Integration: ExtractOrderLineItems",
                                        $"Extract OrderLineItems from Stage.SFCC_OrdersFile into {ordersSchema}.{ordersTable}", e.Message);

                // send email alert
                emailAlertQueue.AddMessage(new CloudQueueMessage($"Azure WebJob - SFCC Integration: ExtractOrderLineItems;{e};{Convert.ToString(processKey)};;;"));

                // Return fail flag
                return 'N';
            }

            // Initialize ProcessSubtaskKey
            long? processSubtaskKey = null;

            List<OrderLineItem> orderLineItems = new List<OrderLineItem>();

            try
            {           
                try
                {
                    logger.log($"Extracting OrderLineItems json from {sourceSchema}.{sourceTable}...");
                    Console.WriteLine($"Extracting OrderLineItems json from {sourceSchema}.{sourceTable}...");

                    // Insert new record into [Audit].[ProcessSubtask] table
                    processSubtaskKey = audit.getInitialProcessSubtaskRecord(processKey, (long)processTaskKey, $"Extract OrderLineItems data and row counts from OrderLineItems Json column in {sourceSchema}.{sourceTable} into {orderLineItemsSchema}.{orderLineItemsTable}",
                                                                                orderLineItemsSchema, orderLineItemsTable, true, sourceSchema, sourceTable, CDCColumnName, false);

                    // Open SQL connection
                    this.conn.Open();

                    // Extract OrderLineItems from [Stage].[SFCC_OrdersFile]
                    string query = $"SELECT [{orderLineItemsColumnName}] FROM [{sourceSchema}].[{sourceTable}]" +
                                    $"WHERE [{orderLineItemsColumnName}] IS NOT NULL;";

                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                orderLineItems.AddRange(JsonConvert.DeserializeObject<List<OrderLineItem>>(reader[0].ToString()));
                            }
                        }
                    }

                    // Close SQL connection
                    this.conn.Close();

                    logger.log($"Successfully extracted OrderLineItems json");
                    Console.WriteLine($"Successfully extracted OrderLineItems json");
                }
                catch (Exception e)
                {
                    logger.log($"OrderLineItems json extraction failed. Details: {e}");
                    Console.WriteLine($"OrderLineItems json extraction failed. Details: {e}");
                    throw new Exception($"OrderLineItems json extraction failed. Details: {e}");
                }

                try
                {
                    logger.log($"Writing OrderLineItems data to {orderLineItemsSchema}.{orderLineItemsTable}...");
                    Console.WriteLine($"Writing OrderLineItems data to {orderLineItemsSchema}.{orderLineItemsTable}...");

                    // Open SQL connection
                    this.conn.Open();

                    // Truncate staging table
                    string query = $"TRUNCATE TABLE [{orderLineItemsSchema}].[{orderLineItemsTable}];";
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        cmd.ExecuteNonQuery();
                    }

                    // Insert records into database
                    DataTable table = CreateDataTable("OrderLineItems", orderLineItems);
                    WriteToDatabase(orderLineItemsSchema, orderLineItemsTable, table);

                    // Close SQL connection
                    this.conn.Close();

                    Console.WriteLine("Write successful");
                    logger.log($"Write successful");
                }
                catch (Exception e)
                {
                    // Update [Audit].[ProcessSubtask] table
                    audit.updateProcessSubtaskRecord((long)processSubtaskKey, 'N', false);

                    Console.WriteLine("Write failed");
                    logger.log($"Failed to write OrderLineItems data to {orderLineItemsSchema}.{orderLineItemsTable}. Details: {e}");

                    throw new Exception($"Failed to write OrderLineItems data to {orderLineItemsSchema}.{orderLineItemsTable}. Details: {e}");
                }

                try
                {
                    logger.log($"Collecting transaction metadata...");
                    Console.WriteLine($"Collecting transaction metadata...");

                    // Retrieve transaction metadata
                    int? sourceRowCount = null;
                    int? insertedRowCount = null;
                    int? finalTargetRowCount = null;
                    
                    // Count source rows
                    // note: the list is counted because the json arrays pulled from this column allow for more than one entry per source row.
                    sourceRowCount = orderLineItems.Count;

                    // Count inserted rows
                    insertedRowCount = sourceRowCount; // All source rows are inserts       

                    // Open SQL connection
                    this.conn.Open();

                    // Count target rows
                    using (SqlCommand cmd = new SqlCommand($"SELECT COUNT(1) FROM [{orderLineItemsSchema}].[{orderLineItemsTable}];", conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                finalTargetRowCount = Convert.ToInt32(reader[0].ToString());
                            }
                        }
                    }

                    // Close SQL connection
                    this.conn.Close();

                    logger.log($"Successfully collected transaction metadata");
                    Console.WriteLine($"Successfully collected transaction metadata");

                    // Update [Audit].[ProcessSubtask] table
                    audit.updateProcessSubtaskRecord((long)processSubtaskKey, 'Y', false, sourceRowCount, 0, (int)finalTargetRowCount, insertedRowCount, 0);
                }
                catch (Exception e)
                {
                    logger.log($"Transaction metadata collection failed. Details: {e}");
                    Console.WriteLine($"Transaction metadata collection failed. Details: {e}");

                    throw new Exception($"Transaction metadata collection failed. Details: {e}");
                }
            }
            catch (Exception e)
            {
                logger.log($"ExtractOrderLineItems failed to execute successfully.");
                Console.WriteLine($"ExtractOrderLineItems failed to execute successfully.");

                // Update [Audit].[ProcessSubtask] table
                audit.updateProcessSubtaskRecord((long)processSubtaskKey, 'N', false);

                // update [Audit].[ProcessTask] table
                audit.updateProcessTaskRecord((long)processTaskKey, 'N');

                // Write to [Audit].[ErrorLog] table
                audit.writeToErrorLog(processKey, processTaskKey, null, processSubtaskKey, "Azure WebJob - SFCC Integration: ExtractOrderLineItems",
                                        "Extract OrderLineItems from Stage.SFCC_OrdersFile into SFCC.OrderLineItems", e.Message);

                // Send email
                emailAlertQueue.AddMessage(new CloudQueueMessage($"Azure WebJob - SFCC Integration: ExtractOrderLineItems;{e};{Convert.ToString(processKey)};;{processTaskKey};{processSubtaskKey}"));

                // Return fail flag
                return 'N';
            }
            finally
            {
                // Close SQL connection
                this.conn.Close();
            }

            try
            {
                logger.log($"Executing {SPMergeOrderLineItems}...");
                Console.WriteLine($"Executing {SPMergeOrderLineItems}...");

                // Insert new record into [Audit].[ProcessSubtask] table
                processSubtaskKey = audit.getInitialProcessSubtaskRecord(processKey, (long)processTaskKey, $"Merge {orderLineItemsSchema}.{orderLineItemsTable} into SFCC.OrderLineItems",
                                                                            "SFCC", "OrderLineItems", true, orderLineItemsSchema, orderLineItemsTable, CDCColumnName, false);

                // Open SQL connection
                this.conn.Open();

                // Execute merge stored procedure
                using (SqlCommand command = new SqlCommand(SPMergeOrderLineItems, conn))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@AuditProcessSubtaskKey", processSubtaskKey));
                    command.Parameters.Add(new SqlParameter("@SourceHasAuditColumnsFlag", false));

                    command.ExecuteNonQuery();
                }

                // Close SQL connection
                this.conn.Close();

                logger.log($"Merge execution successful");
                Console.WriteLine($"Merge execution successful");

                // Update [Audit].[ProcessTask] table
                audit.updateProcessTaskRecord((long)processTaskKey, 'Y');

                logger.log($"ExtractOrderLineItems completed successfully");
                Console.WriteLine($"ExtractOrderLineItems completed successfully");

                // Return success flag
                return 'Y';
            }
            catch (Exception e)
            {
                // log exception
                logger.log($"Merge execution failed. Error: {e}");
                Console.WriteLine($"Merge execution failed. Error: {e}");

                // Update [Audit].[ProcessSubtask] table
                audit.updateProcessSubtaskRecord((long)processSubtaskKey, 'N', false);

                // update [Audit].[ProcessTask] table
                audit.updateProcessTaskRecord((long)processTaskKey, 'N');

                // Write to [Audit].[ErrorLog] table
                audit.writeToErrorLog(processKey, processTaskKey, null, processSubtaskKey, "Azure WebJob - SFCC Integration: ExtractOrderLineItems",
                                        "Extract OrderLineItems from Stage.SFCC_OrdersFile into SFCC.OrderLineItems", e.Message);

                // send email
                emailAlertQueue.AddMessage(new CloudQueueMessage($"Azure WebJob - SFCC Integration: ExtractOrderLineItems;{e};{Convert.ToString(processKey)};;{processTaskKey};{processSubtaskKey}"));

                // Return fail flag
                return 'N';
            }
            finally
            {
                // Close SQL connection
                this.conn.Close();
            }
        }

        public char ExtractOrderPayments()
        {
            // Initialize ProcessTask key
            long? processTaskKey = null;
            try
            {
                // Insert new record into [Audit].[ProcessTask] table
                processTaskKey = audit.getInitialProcessTaskRecord(processKey, $"Extract OrderPayments from {sourceSchema}.{sourceTable} into SFCC.OrderPayments");
            }
            catch (Exception e)
            {
                logger.log($"Failed to initialize ProcessTask record for ExtractOrderPayments parsing function.");
                Console.WriteLine($"Failed to initialize ProcessTask record for ExtractOrderPayments parsing function.");

                // Write to [Audit].[ErrorLog] table
                audit.writeToErrorLog(processKey, processTaskKey, null, null, "Azure WebJob - SFCC Integration: ExtractOrderPayments",
                                        $"Extract OrderPayments from Stage.SFCC_OrdersFile into {ordersSchema}.{ordersTable}", e.Message);

                // send email alert
                emailAlertQueue.AddMessage(new CloudQueueMessage($"Azure WebJob - SFCC Integration: ExtractOrderPayments;{e};{Convert.ToString(processKey)};;;"));

                // Return fail flag
                return 'N';
            }


            // Initialize ProcessSubtaskKey
            long? processSubtaskKey = null;

            List<OrderPayment> orderPayments = new List<OrderPayment>();

            try
            {               
                try
                {
                    logger.log($"Extracting OrderPayments json from {sourceSchema}.{sourceTable}...");
                    Console.WriteLine($"Extracting OrderPayments json from {sourceSchema}.{sourceTable}...");

                    // Insert new record into [Audit].[ProcessSubtask] table
                    processSubtaskKey = audit.getInitialProcessSubtaskRecord(processKey, (long)processTaskKey, $"Extract OrderPayments data and row counts from OrderPayments Json column in {sourceSchema}.{sourceTable} into {orderPaymentsSchema}.{orderPaymentsTable}",
                                                                                orderPaymentsSchema, orderPaymentsTable, true, sourceSchema, sourceTable, CDCColumnName, false);
                    
                    // Open SQL connection
                    this.conn.Open();

                    // Extract OrderPayments from [Stage].[SFCC_OrdersFile]
                    string query = $"SELECT [{orderPaymentsColumnName}] FROM [{sourceSchema}].[{sourceTable}]" +
                                    $"WHERE [{orderPaymentsColumnName}] IS NOT NULL;";

                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                orderPayments.AddRange(JsonConvert.DeserializeObject<List<OrderPayment>>(reader[0].ToString()));
                            }
                        }
                    }

                    // Close SQL connection
                    this.conn.Close();

                    logger.log($"Successfully extracted OrderPayments json");
                    Console.WriteLine($"Successfully extracted OrderPayments json");
                }
                catch (Exception e)
                {
                    logger.log($"OrderPayments json extraction failed. Details: {e}");
                    Console.WriteLine($"OrderPayments json extraction failed. Details: {e}");

                    throw new Exception($"OrderPayments json extraction failed. Details: {e}");
                }

                try
                {
                    logger.log($"Writing OrderPayments data to {orderPaymentsSchema}.{orderPaymentsTable}...");
                    Console.WriteLine($"Writing OrderPayments data to {orderPaymentsSchema}.{orderPaymentsTable}...");

                    // Open SQL connection
                    this.conn.Open();

                    // Truncate staging table
                    string query = $"TRUNCATE TABLE [{orderPaymentsSchema}].[{orderPaymentsTable}];";
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        cmd.ExecuteNonQuery();
                    }

                    // Insert records into database
                    DataTable table = CreateDataTable("OrderPayments", orderPayments);
                    WriteToDatabase(orderPaymentsSchema, orderPaymentsTable, table);

                    // Close SQL connection
                    this.conn.Close();

                    Console.WriteLine("Write succeeded");
                    logger.log($"Write succeeded");
                }
                catch (Exception e)
                {
                    // Update [Audit].[ProcessSubtask] table
                    audit.updateProcessSubtaskRecord((long)processSubtaskKey, 'N', false);

                    logger.log($"Failed to write OrderPayments data to {orderPaymentsSchema}.{orderPaymentsTable}. Details: {e}");
                    Console.WriteLine($"Failed to write OrderPayments data to {orderPaymentsSchema}.{orderPaymentsTable}. Details: {e}");

                    throw new Exception($"Failed to insert OrderPayments data into {orderPaymentsSchema}.{orderPaymentsTable}. Details: {e}");
                }

                try
                {
                    logger.log($"Collecting transaction metadata...");
                    Console.WriteLine($"Collecting transaction metadata...");

                    // Retrieve transaction metadata
                    int? sourceRowCount = null;
                    int? insertedRowCount = null;
                    int? finalTargetRowCount = null;

                    // Count source rows
                    // note: the list is counted because the json arrays pulled from this column allow for more than one entry per source row.
                    sourceRowCount = orderPayments.Count;

                    // Count inserted rows
                    insertedRowCount = sourceRowCount; // All source rows are inserts

                    // Open SQL connection
                    this.conn.Open();

                    // Count target rows
                    using (SqlCommand cmd = new SqlCommand($"SELECT COUNT(1) FROM [{orderPaymentsSchema}].[{orderPaymentsTable}];", conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                finalTargetRowCount = Convert.ToInt32(reader[0].ToString());
                            }
                        }
                    }

                    // Close SQL connection
                    this.conn.Close();

                    logger.log($"Successfully collected transaction metadata");
                    Console.WriteLine($"Successfully collected transaction metadata");

                    // Update [Audit].[ProcessSubtask] table
                    audit.updateProcessSubtaskRecord((long)processSubtaskKey, 'Y', false, sourceRowCount, 0, (int)finalTargetRowCount, insertedRowCount, 0);
                }
                catch (Exception e)
                {
                    logger.log($"Transaction metadata collection failed. Details: {e}");
                    Console.WriteLine($"Transaction metadata collection failed. Details: {e}");

                    throw new Exception($"Transaction metadata collection failed. Details: {e}");
                }
            }
            catch (Exception e)
            {
                logger.log($"Azure WebJob - SFCC Integration: ExtractOrderPayments failed to execute successfully.");
                Console.WriteLine($"Azure WebJob - SFCC Integration: ExtractOrders failed to execute successfully.");

                // Update [Audit].[ProcessSubtask] table
                audit.updateProcessSubtaskRecord((long)processSubtaskKey, 'N', false);

                // update [Audit].[ProcessTask] table
                audit.updateProcessTaskRecord((long)processTaskKey, 'N');

                // Write to [Audit].[ErrorLog] table
                audit.writeToErrorLog(processKey, processTaskKey, null, processSubtaskKey, "Azure WebJob - SFCC Integration: ExtractOrderPayments",
                                        $"Extract OrderPayments from {sourceSchema}.{sourceTable} into SFCC.OrderPayments", e.Message);

                // send email
                emailAlertQueue.AddMessage(new CloudQueueMessage($"Azure WebJob - SFCC Integration: ExtractOrderPayments;{e};{Convert.ToString(processKey)};;{processTaskKey};{processSubtaskKey}"));

                // Return fail flag
                return 'N';
            }
            finally
            {
                // Close SQL connection
                this.conn.Close();
            }

            try
            {
                logger.log($"Executing ETL.Merge_SFCC_OrderPayments...");
                Console.WriteLine($"Executing ETL.Merge_SFCC_OrderPayments...");

                // Insert new record into [Audit].[ProcessSubtask] table
                processSubtaskKey = audit.getInitialProcessSubtaskRecord(processKey, (long)processTaskKey, $"Merge {orderPaymentsSchema}.{orderPaymentsTable} into SFCC.OrderPayments",
                                                                            "SFCC", "OrderPayments", true, orderPaymentsSchema, orderPaymentsTable, CDCColumnName, false);

                // Open SQL connection
                this.conn.Open();

                using (SqlCommand command = new SqlCommand(SPMergeOrderPayments, conn))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@AuditProcessSubtaskKey", processSubtaskKey));
                    command.Parameters.Add(new SqlParameter("@SourceHasAuditColumnsFlag", false));

                    command.ExecuteNonQuery();
                }

                logger.log($"Merge execution successful");
                Console.WriteLine($"Merge execution successful");

                // Close SQL connection
                this.conn.Close();

                // Update [Audit].[ProcessTask] table
                audit.updateProcessTaskRecord((long)processTaskKey, 'Y');

                logger.log($"ExtractOrderPayments completed successfully");
                Console.WriteLine($"ExtractOrderPayments completed successfully");

                // Return success flag
                return 'Y';
            }
            catch (Exception e)
            {
                // log exception
                logger.log($"Merge execution failed. Error: {e}");
                Console.WriteLine($"Merge execution failed. Error: {e}");

                // Update [Audit].[ProcessSubtask] table
                audit.updateProcessSubtaskRecord((long)processSubtaskKey, 'N', false);

                // update [Audit].[ProcessTask] table
                audit.updateProcessTaskRecord((long)processTaskKey, 'N');

                // Write to [Audit].[ErrorLog] table
                audit.writeToErrorLog(processKey, processTaskKey, null, processSubtaskKey, "Azure WebJob - SFCC Integration: ExtractOrderPayments",
                                        $"Extract OrderPayments from {orderPaymentsSchema}.{orderPaymentsTable} into SFCC.OrderPayments", e.Message);

                // send email
                emailAlertQueue.AddMessage(new CloudQueueMessage($"Azure WebJob - SFCC Integration: ExtractOrderPayments;{e};{Convert.ToString(processKey)};;{processTaskKey};{processSubtaskKey}"));

                // Return fail flag
                return 'N';
            }
            finally
            {
                // Close SQL connection
                this.conn.Close();
            }
        }

        private void WriteToDatabase(string targetSchema, string targetTable, DataTable table)
        {            
            SqlBulkCopy copier = new SqlBulkCopy(conn);

            // Set bulk copy timeout and batch size
            copier.BulkCopyTimeout = timeout;
            copier.BatchSize = batchsize;

            // Set target
            copier.DestinationTableName = $"{targetSchema}.{targetTable}";

            // Map data table columns to target table columns
            switch (targetTable)
            {
                case "SFCC_OrderLineItems":
                    copier.ColumnMappings.Add(new SqlBulkCopyColumnMapping("LineItemID", "LineItemID"));
                    copier.ColumnMappings.Add(new SqlBulkCopyColumnMapping("OrderNo", "OrderNo"));
                    copier.ColumnMappings.Add(new SqlBulkCopyColumnMapping("Description", "Description"));
                    copier.ColumnMappings.Add(new SqlBulkCopyColumnMapping("DiscountPrice", "DiscountPrice"));
                    //copier.ColumnMappings.Add(new SqlBulkCopyColumnMapping("ImageURL", "ImageUrl"));
                    copier.ColumnMappings.Add(new SqlBulkCopyColumnMapping("ItemNo", "ItemNo"));
                    //copier.ColumnMappings.Add(new SqlBulkCopyColumnMapping("ProductURL", "ProductUrl"));
                    copier.ColumnMappings.Add(new SqlBulkCopyColumnMapping("QuantityOrdered", "QuantityOrdered"));
                    copier.ColumnMappings.Add(new SqlBulkCopyColumnMapping("TotalLineAmount", "TotalLineAmount"));
                    copier.ColumnMappings.Add(new SqlBulkCopyColumnMapping("UnitPrice", "UnitPrice"));
                    break;
                case "SFCC_OrderPayments":
                    copier.ColumnMappings.Add(new SqlBulkCopyColumnMapping("PaymentId", "PaymentId"));
                    copier.ColumnMappings.Add(new SqlBulkCopyColumnMapping("OrderNo", "OrderNo"));
                    //copier.ColumnMappings.Add(new SqlBulkCopyColumnMapping("CCLast4", "CCLast4"));
                    copier.ColumnMappings.Add(new SqlBulkCopyColumnMapping("CCType", "CCType"));
                    copier.ColumnMappings.Add(new SqlBulkCopyColumnMapping("PaymentMethod", "PaymentMethod"));
                    copier.ColumnMappings.Add(new SqlBulkCopyColumnMapping("PaymentSubtotal", "PaymentSubtotal"));
                    copier.ColumnMappings.Add(new SqlBulkCopyColumnMapping("BillingFirstName", "BillingFirstName"));
                    copier.ColumnMappings.Add(new SqlBulkCopyColumnMapping("BillingLastName", "BillingLastName"));
                    copier.ColumnMappings.Add(new SqlBulkCopyColumnMapping("BillingAddress", "BillingAddress"));
                    copier.ColumnMappings.Add(new SqlBulkCopyColumnMapping("BillingAddress2", "BillingAddress2"));
                    copier.ColumnMappings.Add(new SqlBulkCopyColumnMapping("BillingAddressCity", "BillingAddressCity"));
                    copier.ColumnMappings.Add(new SqlBulkCopyColumnMapping("BillingAddressState", "BillingAddressState"));
                    copier.ColumnMappings.Add(new SqlBulkCopyColumnMapping("BillingAddressPostalCode", "BillingAddressPostalCode"));
                    copier.ColumnMappings.Add(new SqlBulkCopyColumnMapping("BillingAddressCountry", "BillingAddressCountry"));
                    copier.ColumnMappings.Add(new SqlBulkCopyColumnMapping("BillingAddressPhone", "BillingAddressPhone"));
                    break;
            }

            copier.WriteToServer(table);
        }

        private DataTable CreateDataTable(string source, object data)
        {
            DataTable table = new DataTable();

            switch (source)
            {
                case "OrderLineItems":
                    List<OrderLineItem> orderLineItems = (List<OrderLineItem>)data;

                    // Add columns
                    table.Columns.Add("LineItemID", typeof(string));
                    table.Columns.Add("OrderNo", typeof(string));
                    table.Columns.Add("Description", typeof(string));
                    table.Columns.Add("DiscountPrice", typeof(decimal));
                    //table.Columns.Add("ImageURL", typeof(object));
                    table.Columns.Add("ItemNo", typeof(string));
                    //table.Columns.Add("ProductURL", typeof(object));
                    table.Columns.Add("QuantityOrdered", typeof(int));
                    table.Columns.Add("TotalLineAmount", typeof(decimal));
                    table.Columns.Add("UnitPrice", typeof(decimal));

                    // Add rows
                    foreach (OrderLineItem item in orderLineItems)
                    {
                        table.Rows.Add(item.LineItemID,
                                        item.OrderNo,
                                        item.Description,
                                        item.DiscountPrice,
                                        // item.ImageURL, 
                                        item.ItemNo,
                                        // item.ProductURL, 
                                        item.QuantityOrdered,
                                        item.TotalLineAmount,
                                        item.UnitPrice);
                    }
                    break;

                case "OrderPayments":

                    List<OrderPayment> orderPayments = (List<OrderPayment>)data;

                    // Add columns
                    table.Columns.Add("PaymentId", typeof(string));
                    table.Columns.Add("OrderNo", typeof(string));
                    // table.Columns.Add("CCLast4", typeof(string));
                    table.Columns.Add("CCType", typeof(string));
                    table.Columns.Add("PaymentMethod", typeof(string));
                    table.Columns.Add("PaymentSubtotal", typeof(string));
                    table.Columns.Add("BillingFirstName", typeof(string));
                    table.Columns.Add("BillingLastName", typeof(string));
                    table.Columns.Add("BillingAddress", typeof(string));
                    table.Columns.Add("BillingAddress2", typeof(string));
                    table.Columns.Add("BillingAddressCity", typeof(string));
                    table.Columns.Add("BillingAddressState", typeof(string));
                    table.Columns.Add("BillingAddressPostalCode", typeof(string));
                    table.Columns.Add("BillingAddressCountry", typeof(string));
                    table.Columns.Add("BillingAddressPhone", typeof(string));

                    // Add rows
                    foreach (OrderPayment payment in orderPayments)
                    {
                        table.Rows.Add(payment.PaymentId,
                                        payment.OrderNo,
                                         // payment.CCLast4, 
                                        payment.CCType,
                                        payment.PaymentMethod,
                                        payment.PaymentSubtotal,
                                        payment.BillingFirstName,
                                        payment.BillingLastName,
                                        payment.BillingAddress,
                                        payment.BillingAddress2,
                                        payment.BillingAddressCity,
                                        payment.BillingAddressState,
                                        payment.BillingAddressPostalCode,
                                        payment.BillingAddressCountry,
                                        payment.BillingAddressPhone
                                        );
                    }
                    break;
            }

            return table;
        }
    }
}
