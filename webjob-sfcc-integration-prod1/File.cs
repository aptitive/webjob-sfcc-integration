﻿namespace webjob_sfcc_integration_prod1
{    
    /// File class is used to collect file metadata during integration
    public class File
    {
        public string Name { get; set; }
        public string RootName { get; set; }
        public string Size { get; set; }
        public string Type { get; set; }
        public string Location { get; set; }
        public long? AuditProcessFileTaskKey { get; set; }
        public string Pipeline { get; set; }
        public bool SkipFlag { get; set; }        
    }
}
