﻿using System.Collections.Generic;

namespace webjob_sfcc_integration_prod1
{
    // Used during json parameter deserialization
    public class ParameterObject
    {
        public string Name { get; set; }
        public Dictionary<string, object> Parameters { get; set; }
    }
}
